module CommonRegex
  EMAIL = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  PASSWORD = /^.{8,}$/
end

