require 'ostruct'

class RouteNode
  attr_accessor :part, :routes, :children

  def initialize(part)
    @part = part
    @routes = []
    @children = {}
  end

  def print_text(prefix = '', depth = 0)
    unless @part.blank?
      prefix += "/#{@part}"

      unless @routes.count == 0
        puts "#{' ' * depth}#{prefix}"
        depth += 1
      end
    end

    @routes.each do |route|
      controller = route.requirements[:controller]
      controller_class = "#{controller}_controller".camelize.constantize
      action = route.requirements[:action]

      verb = route.verb.source.gsub(/[\^\$]/, '')
      path = [route.optimized_path].flatten.join

      print "#{' ' * (depth)}"
      puts "#{verb} #{path}"
#      puts "#{verb} #{path} (#{controller_class.action_docs[action][:name]})"
#
#      unless controller_class.action_docs[action][:desc].blank?
#        print "#{' ' * (depth)}"
#        puts "Description: #{controller_class.action_docs[action][:desc]}"
#        puts
#      end

      unless controller_class.validations[action].blank?
        puts "#{' ' * (depth)}Parameters:"
        controller_class.validations[action].each do |param_key, validators|
          print "#{' ' * (depth + 1)}"
          puts "#{param_key}"
          validators[:validations].each do |type, validation|
            print "#{' ' * (depth + 2)}"
            puts "#{type}: #{validation.options}"
          end
        end
      end

#      unless controller_class.confirmations[action].empty?
#        puts "#{' ' * (depth)}Confirmation Parameters:"
#        controller_class.confirmations[action].each do | confirmation_hash |
#          print "#{' ' * (depth + 1)}"
#          puts "For updating #{confirmation_hash[:field].blank? ? "all fields" : confirmation_hash[:field]}:"
#          confirmation_hash[:confirm_fields].each do | field |
#            print "#{' ' * (depth + 2)}"
#            puts field.to_s
#          end
#        end
#      end
#
#      unless controller_class.action_docs[action][:error].empty?
#        errors = controller_class.action_docs[action][:error].sort do | x, y |
#          x.keys.first.code <=> y.keys.first.code
#        end
#
#        puts "#{' ' * (depth)}Errors:"
#        errors.each do | error |
#          error_class = error.keys.first
#          description = error.values.first
#          print "#{' ' * (depth + 1)}"
#          puts "#{Rack::Utils.status_code(error_class.status_code)}: #{error_class.code}: #{description}"
#        end
#        puts
#      end
      puts
    end

    @children.each do |part, child|
      child.print_text(prefix, depth)
    end
  end
end

desc 'Generate api documentation'
namespace :doc do
  task api: :environment do
    routes = RouteNode.new('')

    Rails.application.routes.routes.each do |route|
      # skip sidekiq admin
      next if route.name == 'sidekiq_web'

      controller = route.requirements[:controller]
      next if controller.nil?
      controller_class = "#{controller}_controller".camelize.constantize

      # skip rails default routes
      next if controller_class.to_s =~ /^Rails/

      verb = route.verb.source.gsub(/[\^\$]/, '')
      path = [route.optimized_path].flatten.join

      route_signature = "#{path[1..-1].gsub(/:[^\/]+/, '')}"

      parts = route_signature.split('/')
      current_node = routes
      parts.each do |part|
        current_node.children[part] ||= RouteNode.new(part)
        current_node = current_node.children[part]
      end
      current_node.routes << route
    end

    routes.print_text
  end
end
