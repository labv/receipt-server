module ApiAuth
  class Authorization
    AUTH_TYPE = 'ApiToken'
    attr_accessor :token, :session_id, :user_id, :auth_type

    def initialize(token, session_id, user_id, auth_type = AUTH_TYPE)
      self.token = token
      self.auth_type = auth_type
      self.session_id = session_id
      self.user_id = user_id

      if token.nil? || session_id.nil? || user_id.nil?
        raise InvalidSessionError.new
      end
    end

    def to_s
      "#{self.auth_type} uid='#{self.user_id}', sid='#{self.session_id}', token='#{self.token}'"
    end

    def self.parse(auth_header, auth_type = AUTH_TYPE)
      type, *others = auth_header.split
      if type.strip != auth_type
        raise InvalidSessionError.new
      end

      fields = Hash[others.map do |f|
        f.split('=').map do |t|
          t.strip.gsub(/,$/, '').gsub(/'/, '').gsub(/"/, '')
        end
      end].with_indifferent_access

      Authorization.new(fields[:token], fields[:sid], fields[:uid], auth_type)
    rescue
      raise InvalidSessionError.new
    end
  end
end
