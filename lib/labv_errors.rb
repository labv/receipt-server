module LabvErrors
  def all_exceptions
    self.constants.select{ |c| Class === self.const_get(c) }.map { |c| c.to_s }
  end

  module ErrorHelpers
    def to_hash
      errors = @errors
      if errors && errors.is_a?(Hash)
        errors = errors.reduce({}) do |hash, (key, value)|
          hash[key] = value.is_a?(Array) ? value.first.to_s : value.to_s
          hash
        end
      end

      response = { code: self.class.name.demodulize.underscore }
      unless Rails.env == 'production'
        response[:debug_info] = errors || { "unknown" => self.message }
      end
      response
    end

    def initialize(message)
      super(message)
    end

    def status_code
      self.class::STATUS_CODE
    end
  end

  class LabvBaseError < StandardError
    include ErrorHelpers

    STATUS_CODE = :internal_server_error
  end

  class LabvArgumentError < ArgumentError
    include ErrorHelpers

    STATUS_CODE = :bad_request
  end
end

Dir[File.dirname(__FILE__) + "/labv_errors/*.rb"].each {|file| require file }
