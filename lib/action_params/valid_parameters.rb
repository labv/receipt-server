module ActionParams
  module ValidParameters
    module ActionController
      module Metal
        def self.included(base)
          base.class_eval do
            extend ClassMethods
            prepend InstanceMethods
          end
        end

        module ClassMethods
          attr_reader :validations

          def method_added(name)
            @validations ||= {}.with_indifferent_access
            @validations[name] = @next_validation
            @next_validation = {}
            super(name)
          end

          def validates(param_key, options = {})
            raise ArgumentError, 'must specify at least one validation' if options.empty?

            @next_validation ||= {}
            @next_validation[param_key] = {}

            @next_validation[param_key][:transformations] ||= {}
            if options[:transform]
              options[:transform].each do |transformation, value|
                klass = ActionParams::ValidParameters::Transformations.const_get "#{transformation.to_s.camelize}"
                @next_validation[param_key][:transformations][transformation] = klass.new(value)
              end

              options = options.dup
              options.delete(:transform)
            end

            @next_validation[param_key][:validations] ||= {}
            options.each do |validation, value|
              klass = ActionParams::ValidParameters::Validations.const_get "#{validation.to_s.camelize}"
              @next_validation[param_key][:validations][validation] = klass.new(value)
            end
          end
        end

        module InstanceMethods
          def process_action(name, *args)
            validate_params(name, request.params)
            super(name, *args)
          end

          def validate_params(action, params)
            all_errors = {}
            valid_params = {}.with_indifferent_access

            self.class.validations ||= {}
            self.class.validations[action] ||= {}
            self.class.validations[action].each do |param_key, processors|
              processors[:transformations].each do |transformation_name, transformer|
                params[param_key] = transformer.transform(params[param_key])
              end

              validators = processors[:validations]
              param = params[param_key]

              next if param.nil? && (validators[:required].nil? || validators[:required].options == false)
              raise InvalidParametersError.new({ param_key => ["missing"] }) if param.nil?

              errors = []

              validators.each do |validation, validator|
                errors += validator.errors unless validator.valid?(param)
              end

              all_errors[param_key] = errors if errors.present?

              valid_params[param_key] = param
            end

            raise InvalidParametersError.new(all_errors) unless all_errors.empty?

            @valid_params = valid_params
          end

          protected

          attr_reader :valid_params
        end
      end
    end

    module Transformations
      class Transformer
        attr_reader :options

        def initialize(options)
          @options = options
        end
      end

      class Custom < Transformer
        def transform(param)
          @options.call(param)
        end
      end

      class Default < Transformer
        def transform(param)
          param || @options
        end
      end

      class Bool < Transformer
        def transform(param)
          param.to_s.to_bool
        end
      end

      class Date < Transformer
        def transform(param)
          ::Date.parse(param) rescue nil
        end
      end

      class DateTime < Transformer
        def transform(param)
          ::DateTime.parse(param) rescue nil
        end
      end
    end

    module Validations
      class Validator
        attr_reader :errors, :options

        def initialize(options)
          @errors = []
          @options = options
        end

        def valid?(param)
          @errors = []
          self.validate(param)
          @errors.empty?
        end
      end

      class Nested < Validator
        def initialize(options)
          super(options)
          @validators = {}

          options.each do |nested_key, nested_validators|
            @validators[nested_key] = {}

            @validators[nested_key][:transformations] ||= {}
            if nested_validators[:transform]
              nested_validators[:transform].each do |transformation, value|
                klass = ActionParams::ValidParameters::Transformations.const_get "#{transformation.to_s.camelize}"
                @validators[nested_key][:transformations][transformation] = klass.new(value)
              end

              nested_validators = nested_validators.dup
              nested_validators.delete(:transform)
            end

            @validators[nested_key][:validations] ||= {}
            nested_validators.each do |nested_validator, value|
              klass = ActionParams::ValidParameters::Validations.const_get "#{nested_validator.to_s.camelize}"
              @validators[nested_key][:validations][nested_validator] = klass.new(value)
            end
          end
        end

        def validate(param)
          @validators.each do |nested_key, processors|
            transformations = processors[:transformations]
            transformations.each do |transformation_name, transformer|
              param[nested_key] = transformer.transform(param[nested_key])
            end

            validators = processors[:validations]
            nested_param = param[nested_key]
            next if nested_param.nil? && (validators[:required].nil? || validators[:required] == false)
            validators.each do |validation, validator|
              unless validator.valid?(nested_param)
                @errors += validator.errors.map { |error| "#{nested_key}: #{error}" }
              end
            end
          end

          param
        end
      end

      class Custom < Validator
        def validate(param)
          @errors << 'invalid' unless @options.call(param)

          param
        end
      end

      class Required < Validator
        def validate(param)
          if options == true
            @errors << 'missing' if param.nil?
          end

          param
        end
      end

      class Type < Validator
        def validate(param)
          @errors << "is not an array" unless param.is_a? options
        end
      end

      class In < Validator
        def validate(param)

          unless @options.map(&:to_s).include? param.to_s
            @errors << 'not valid'
          end

          param
        end
      end

      class Numericality < Validator
        def validate(param)
          unless param = (Integer(param) rescue nil) || (Float(param) rescue nil)
            @errors << 'not numeric'
            return param
          end

          return param if @options == true

          if @options[:greater_than] && param < @options[:greater_than]
            @errors << 'too small'
          end

          if @options[:less_than] && param > @options[:less_than]
            @errors << 'too large'
          end

          param
        end
      end

      class Format < Validator
        def validate(param)
          unless param.is_a?(String)
            @errors << 'not a string'
            return param
          end

          format = if @options.is_a? Regexp
                     @options
                   else
                    @options[:with]
                   end
          if format && !(param =~ format)
            @errors << "does not match #{format.to_s}"
          end

          param
        end
      end

      class Length < Validator
        def validate(param)
          unless param.is_a?(String)
            @errors << 'not a string'
            return param
          end

          if @options[:min] && param.length < @options[:min]
            @errors << 'too short'
          end

          if @options[:max] && param.length > @options[:max]
            @errors << 'too long'
          end

          param
        end
      end
    end
  end
end

ActionController::Metal.send :include, ActionParams::ValidParameters::ActionController::Metal

