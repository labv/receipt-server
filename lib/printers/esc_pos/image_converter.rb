module Printers::EscPos
  class ImageConverter

    # Convert EscPos Rasterized bit image stream to bit array
    #
    # @param [Array<Integer>] Input stream
    # @param [Integer] # of pixels in x axis
    # @param [Integer] # of pixels in y axis
    # @param [Integer] x axis multiplier
    # @param [Integer] y axis multiplier
    #
    # @return [Array<Array<Boolean>>] 2-D bit image array
    def convert(input, width, height, x_multiplier, y_multiplier)
      output = []

      if width / 8.0 != width / 8
        raise "Width (#{width}) is not a multiple of 8"
      end

      expected_length = width / 8 * height
      if expected_length != input.length
        raise "Incorrect length. " \
          "Expected: #{expected_length}." \
          "Got: #{input.length}."
      end

      b = input.in_groups_of(width / 8, 0)
      b.each do |row|
        y_multiplier.times do
          output << []
          row.each do |item|
            item ||= 0
            8.times do |a|
              i = 7 - a
              x_multiplier.times do
                output.last << (((1 << i) & item) != 0)
              end
            end
          end
        end
      end

      output
    end

    # Convert bit array to Magick Image object
    def self.to_magick(bit_array, width, height, transparent = true)
      image = Magick::Image.new(width, height) do |c|
        c.background_color = transparent ? 'Transparent' : 'white'
      end
      image.format = 'png'
      bit_array.each_with_index do |row, y|
        row.each_with_index do |bit, x|
          if bit
            image.pixel_color(x, y, 'black')
          end
        end
      end

      image
    end
  end
end

