module Printers::EscPos::Pdf
  class PdfPrinter
    include Printers::EscPos::CharacterMap

    DEFAULT_PAGE_SIZE = [315, 999999]
    FONT = 'Courier'
    BASE_PATH = '/var/www/files/'

    def initialize(logger = nil)
      @logger = logger || Rails.logger
    end

    def convert_as_file(raw_content, path = nil, filename = nil)
      dir = "#{BASE_PATH || path}#{Date.today.strftime("%Y%m%d")}/"
      Dir.mkdir(dir) unless Dir.exist?(dir)

      file_path = "#{dir}#{filename || SecureRandom.uuid.gsub(/-/, '')}.pdf"
      pdf.render_file file_path

      file_path
    end

    def convert(raw_content)
      pdf = _convert(raw_content)
      pdf.render
    end

    private

    def _convert(raw_content)
      result_pdf = nil
      y = nil

      # Calculate height on first time
      2.times do
        text_buffer = Printers::EscPos::TextBuffer.new
        text_options = {}
        content = raw_content.each_byte

        pdf = if y.present?
                page_size = DEFAULT_PAGE_SIZE
                page_size[1] = y
                Prawn::Document.new(:page_size => page_size)
              else
                Prawn::Document.new(:page_size => DEFAULT_PAGE_SIZE)
              end

        #pdf.font "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf"
        pdf.font FONT
        begin
          while(true)
            byte = content.next
            args = []
            klass = nil

            case byte
            when FF, LF, CR
              klass = PrintAndFeedLine
            when ESC
              command = content.next
              klass = Esc.to_processor(command)
            when GS
              command = content.next
              klass = Gs.to_processor(command)
            when DLE, CAN, FS
              # TODO
            when HT
              text_buffer << HT
            else
              text_buffer << byte
            end

            if klass.present?
              klass::ARG_LENGTH.times do
                args << content.next
              end

              klass.new(pdf, text_buffer, text_options, @logger).process(*args)
            end
          end
        rescue StopIteration
          # DONE
        end

        if y.nil?
          # first time
          y = DEFAULT_PAGE_SIZE.last - pdf.cursor
        else
          result_pdf = pdf
        end
      end

      result_pdf
    end
  end
end

