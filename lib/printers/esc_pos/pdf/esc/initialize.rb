module Printers::EscPos::Pdf::Esc
  class Initialize < Printers::EscPos::Pdf::BaseCommand
    CODE = '@'
    ARG_LENGTH = 0

    def process(*args)
      @text_options.clear
      @text_buffer.clear
    end
  end
end

