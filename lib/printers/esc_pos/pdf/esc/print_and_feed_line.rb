module Printers::EscPos::Pdf::Esc
  class PrintAndFeedLine < Printers::EscPos::Pdf::BaseCommand
    CODE = 'd'
    ARG_LENGTH = 1

    def process(*args)
      value = args.first || 1

      _print_buffer
      (value - 1).times do
        @pdf.text ''
      end
    end
  end
end

