module Printers::EscPos::Pdf::Esc
  class Justification < Printers::EscPos::Pdf::BaseCommand
    CODE = 'a'
    ARG_LENGTH = 1

    def process(*args)
      value = args.first || 0
      alignment = case value
                  when 0, 48
                    :left
                  when 1, 49
                    :center
                  when 2, 50
                    :right
                  end

      if alignment.present?
        @text_options[:align] = alignment
      end
    end
  end
end

