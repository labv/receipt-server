module Printers::EscPos::Pdf::Esc
  class LineHeight < Printers::EscPos::Pdf::BaseCommand
    CODE = '3'
    ARG_LENGTH = 1

    def process(*args)
      value = args.first || 0
      @pdf.default_leading (value.to_f / 406).in
    end
  end
end

