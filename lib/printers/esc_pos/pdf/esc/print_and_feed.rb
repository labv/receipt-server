module Printers::EscPos::Pdf::Esc
  class PrintAndFeed < Printers::EscPos::Pdf::BaseCommand
    CODE = 'J'
    ARG_LENGTH = 1

    def process(*args)
      value = args.first || 1
      _print_buffer
      @pdf.move_down (value.to_f / 406).in
    end
  end
end

