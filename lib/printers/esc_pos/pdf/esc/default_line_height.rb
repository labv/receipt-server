module Printers::EscPos::Pdf::Esc
  class DefaultLineHeight < Printers::EscPos::Pdf::BaseCommand
    CODE = '2'

    ARG_LENGTH = 0
    DEFAULT_LINE_HEIGHT = 0

    def process(*args)
      @pdf.default_leading DEFAULT_LINE_HEIGHT
    end
  end
end

