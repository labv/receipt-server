module Printers::EscPos::Pdf
  class BaseCommand
    def initialize(pdf, text_buffer, text_options, logger)
      @pdf = pdf
      @text_buffer = text_buffer
      @text_options = text_options
      @logger = logger || Rails.logger
    end

    def process(*args)
      raise 'Not implemented'
    end

    protected

    def _print_buffer
      @pdf.text @text_buffer.to_s, @text_options
    rescue => e
      @logger.error(e.message)
      @logger.error(e.backtrace.join("\n"))
    ensure
      @text_buffer.clear
    end
  end
end

