module Printers::EscPos::Pdf::Gs
  class CharacterSize < Printers::EscPos::Pdf::BaseCommand
    CODE = '!'
    ARG_LENGTH = 1

    DEFAULT_SIZE = 10

    def process(*args)
      # TODO need to figure out how to only stretch one side
      #
      # TODO handle changes half way through the line
      # Use formatted text
      size_option = args.first || 0
      width_op = size_option >> 8
      height_op = size_option & 255

      factor = 2 ** ((width_op + height_op)/2.0)

      @pdf.font_size DEFAULT_SIZE * factor
    end
  end
end

