module Printers::EscPos
  class TextBuffer
    def initialize
      self.clear
    end

    def <<(that)
      append(that)
    end

    def append(that)
      if that.is_a? String
        @content += that.unpack('U' * that.length)
      elsif that.is_a? Array
        @content += that
      elsif that.is_a? Integer
        @content << that
      else
        raise 'Unsupported type'
      end
    end

    def clear
      @content = []
    end

    def content
      @content || []
    end

    def to_s
      self.content.pack('C*')
    end

    def method_missing(m, *args, &block)
      self.content.send(m, args, block)
    end

    def respond_to(m, include_private = false)
      if self.content.respond_to(m, include_private)
        true
      else
        super
      end
    end
  end
end

