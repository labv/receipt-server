module Printers::EscPos::Html
  module Modules
    module PrintBitArray

      def print(bit_array, width, height)
        id = SecureRandom.hex
        @html_output << "<canvas id='#{id}' width='#{width + 50}' height='#{height + 50}'></canvas>\n"
        @html_output << "<script>\n"
        @html_output << "var c = document.getElementById('#{id}');\n"
        @html_output << "var ctx = c.getContext('2d');\n"
        @html_output << "var imgData=ctx.createImageData(#{width},#{height});\n"
        @html_output << "var d = imgData.data;"
        @html_output << "var data=["
        bit_array.each do |row|
          row.each do |bit|
            @html_output << "0,0,0,#{bit ? 255 : 0},"
          end
        end
        @html_output << "];\n"
        @html_output << "for (var i=0,len=d.length;i<len;++i) d[i] = data[i];\n"
        @html_output << "ctx.putImageData(imgData, 0, 0); // at coords 0,0\n"
        @html_output << "</script>\n"
      end

      def print_image(bit_array, width, height)
        image = Printers::EscPos::ImageConverter.to_magick(bit_array, width, height)

        @html_output << "<img src='data:image/png;base64,"
        @html_output << Base64.encode64(image.to_blob)
        @html_output << "'/>"
      end
    end
  end
end

