module Printers::EscPos::Html
  class BaseCommand
    def initialize(options = {})
      @text_buffer = options[:text_buffer]
      @html_output = options[:html_output]
      @block_styles = options[:block_styles]
      @block_buffer = options[:block_buffer]
      @inline_styles = options[:inline_styles]
      @logger = options[:logger] || Rails.logger
    end

    def process(*args)
      type, key, value = process_style(*args)

      if type.present? || key.present? || value.present?
        case type
        when :inline
          if @inline_styles.include? key
            _flush_inline
          end

          @inline_styles[key] = value
        when :block
          @block_styles[key] = value
        else
          raise "Unexpected type"
        end
      end
    end

    def process_style(args)
      # Left blank to be overriden if needed
    end

    protected

    def sanitize_block_buffer(buffer)
      buffer.chars.select(&:valid_encoding?).join.gsub(/[^[:print:]]/) {|x| x.ord}
    end

    def _flush
      if @text_buffer.to_s.present?
        _flush_inline
      end

      if @block_buffer.present?
        block_style= @block_styles.map do |key, value|
          "#{key}: #{value}"
        end.join ';'

        @html_output << "<div style='#{block_style}'>#{@block_buffer.string}</div>"
        @block_buffer.reopen("")
      end
    end

    def _flush_inline
      san_string = sanitize_block_buffer(@text_buffer.to_s)
      styles_html = @inline_styles.map do |key, value|
        "#{key}: #{value}"
      end.join ';'

      @block_buffer << "<span style='blank #{styles_html}'>#{san_string}</span>"
      @text_buffer.clear
    end
  end
end

