module Printers::EscPos::Html
  class HtmlPrinter
    DEFAULT_PAGE_WIDTH = '385px'
    FONT = 'Courier'
    DEFAULT_FONT_SIZE = 12

    def initialize(logger = nil)
      @logger = logger || Rails.logger
    end

    def convert(raw_content)
      _convert(raw_content)
    end

    private

    def _convert(raw_content)
      result_html = <<-HTML
        <html>
        <head>
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
        </head>
        <body
          style='background-color: #d0d0d0; width: #{DEFAULT_PAGE_WIDTH}; font-family: #{FONT}; font-size: #{DEFAULT_FONT_SIZE}pt'>
      HTML

      text_buffer = Printers::EscPos::TextBuffer.new
      block_buffer = StringIO.new
      inline_styles = {}
      block_styles = {}
      content = raw_content.each_byte.to_a

      begin
        byte = content.shift

        while(byte != nil)
          klass = nil
          line_feed = PrintAndFeedLine.new(
            :text_buffer => text_buffer,
            :html_output => result_html,
            :block_buffer => block_buffer,
            :block_styles => block_styles,
            :inline_styles => inline_styles,
            :logger => @logger
          )

          case byte
          when FF, LF, CR
            line_feed.process
          when ESC
            klass = Printers::EscPos::Commands.to_processor(:esc, content)
          when GS
            klass = Printers::EscPos::Commands.to_processor(:gs, content)
          when DLE
            klass = Printers::EscPos::Commands.to_processor(:dle, content)
          when CAN
            klass = Printers::EscPos::Commands.to_processor(:can, content)
          when FS
            klass = Printers::EscPos::Commands.to_processor(:fs, content)
          when HT
            text_buffer << HTML_SPACE
            text_buffer << HTML_SPACE
          when SPACE
            text_buffer << HTML_SPACE
          when '\x00'
          else
            text_buffer << byte
          end

          if klass.present?
            handler = klass.new(
              :html,
              :text_buffer => text_buffer,
              :html_output => result_html,
              :block_buffer => block_buffer,
              :block_styles => block_styles,
              :inline_styles => inline_styles,
              :logger => @logger
            )

            handler.process(content)
          end

          byte = content.shift
        end
      end

      if text_buffer.to_s.present?
        result_html << text_buffer.to_s
      end

      result_html += "</body></html>"
      result_html
    end
  end
end

