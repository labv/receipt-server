module Printers::EscPos::Html::Gs
  class PrintRasterBitImage < Printers::EscPos::Html::BaseCommand
    include Printers::EscPos::Html::Modules::PrintBitArray

    def process(bit_array, width, height)
      print_image(bit_array, width, height)
    end
  end
end

