module Printers::EscPos::Html::Gs
  class CharacterSize < Printers::EscPos::Html::BaseCommand
    CODE = '!'
    ARG_LENGTH = 1

    DEFAULT_SIZE = 10

    def process_style(args)
      size_option = args.first || 0
      width_op = size_option >> 8
      height_op = size_option & 255

      factor = 2 ** ((width_op + height_op)/2.0)

      return :inline, :font_size, "#{DEFAULT_SIZE * factor}pt"
    end
  end
end

