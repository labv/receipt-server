module Printers::EscPos::Html::Esc
  class DefaultLineHeight < Printers::EscPos::Html::BaseCommand
    CODE = '2'

    DEFAULT_LINE_HEIGHT = 110

    def process_style(*args)
      return :block, 'line-height', "#{DEFAULT_LINE_HEIGHT}%"
    end
  end
end

