module Printers::EscPos::Html::Esc
  class LineHeight < Printers::EscPos::Html::BaseCommand
    CODE = '3'
    DEFAULT_LINE_HEIGHT = 110

    def process_style(*args)
      value = args.first || 0
      return :block, 'line-height', "#{DEFAULT_LINE_HEIGHT * value}%"
    end
  end
end

