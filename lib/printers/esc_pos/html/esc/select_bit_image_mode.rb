module Printers::EscPos::Html::Gs
  class SelectBitImageMode < Printers::EscPos::Html::BaseCommand
    include Printers::EscPos::Html::Modules::PrintBitArray

    def process(bit_array, width, height)
      print(bit_array, width, height)
    end
  end
end

