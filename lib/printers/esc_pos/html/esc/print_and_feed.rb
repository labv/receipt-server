module Printers::EscPos::Html::Esc
  class PrintAndFeed < Printers::EscPos::Html::BaseCommand
    CODE = 'J'

    def process(*args)
      value = args.first || 1
      value = 1
      (value - 1).times do
        @text_buffer << HTML_SPACE
        _flush
      end
    end
  end
end

