module Printers::EscPos::Html::Esc
  class Initialize < Printers::EscPos::Html::BaseCommand
    CODE = '@'
    ARG_LENGTH = 0

    def process(*args)
      @inline_styles.clear
      @block_styles.clear
      @text_buffer.clear
      @block_buffer.reopen("")
    end
  end
end

