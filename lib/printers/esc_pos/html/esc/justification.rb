module Printers::EscPos::Html::Esc
  class Justification < Printers::EscPos::Html::BaseCommand
    CODE = 'a'
    ARG_LENGTH = 1

    def process_style(*args)
      value = args.first || 0
      alignment = case value
                  when 0, 48
                    :left
                  when 1, 49
                    :center
                  when 2, 50
                    :right
                  end

      if alignment.present?
        return :block, 'text-align', alignment
      else
        return nil
      end
    end
  end
end

