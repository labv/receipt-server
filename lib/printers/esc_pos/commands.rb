# Load base commands first
require_relative('commands/base')
require_relative('commands/esc')
require_relative('commands/fs')
require_relative('commands/gs')
require_relative('commands/dle')

# Load fixed length commands
require_relative('commands/fixed_length_commands')

module Printers::EscPos::Commands
  class DoNothing < Base
    def process(pointer)
      self.get_output_command.try(:process)
    end
  end
end

module Printers::EscPos
  module Commands
    def self.map
      @map ||= _build_map
      @map
    end

    def self.to_processor(type, pointer)
      @map ||= _build_map
      command_set = @map[type]
      set = command_set

      unless set.nil?
        set, count = self._traverse_code(pointer, 0, set)
        pointer.shift(count+1)
      end

      klass = set
      klass || DoNothing
    end

    private

    def self._traverse_code(pointer, index, set)
      char = pointer[index]
      new_set = set[char] || set[nil]
      result = nil
      found_index = -1

      if new_set.is_a? Hash
        result, new_index = self._traverse_code(pointer, index + 1, new_set)

        unless result.nil?
          found_index = new_index
        end
      else
        result = new_set

        unless result.nil?
          found_index = index
        end
      end

      return result, found_index
    end

    def self._build_map
      classes = _find_classes(self, true)
      result = {}

      classes.each do |c|
        type = c.name.split('::')[-2].downcase.to_sym
        result[type] ||= {}
        hash = result[type]
        code = c.const_get(:CODE).bytes
        code[0..-2].each do |byte|
          hash[byte] ||= {}
          hash = hash[byte]
        end
        if hash[code[-1]].is_a? Hash
          hash[code[-1]][nil] = c
        else
          hash[code[-1]] = c
        end
      end

      result
    end

    def self._find_classes(thing, recurse_module = false)
      classes = []
      thing.constants.map do |c|
        thing.const_get(c)
      end.each do |c|
        if c.is_a?(Class) && c.constants.include?(:CODE)
          classes << c
        elsif c.is_a?(Module) && recurse_module
          # recurse 1 level deep
          classes += _find_classes(c)
        end
      end
      classes
    end
  end
end

Dir[File.dirname(__FILE__) + '/commands/esc/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/commands/fs/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/commands/gs/*.rb'].each {|file| require file }

