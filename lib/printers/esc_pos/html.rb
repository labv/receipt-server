module Printers::EscPos
  module Html
  end
end

require_relative('html/base_command')
require_relative('html/base_module')
require_relative('html/esc')
require_relative('html/gs')
require_relative('html/html_printer')
require_relative('html/print_and_feed_line')

Dir[File.dirname(__FILE__) + '/html/modules/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/html/gs/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/html/esc/*.rb'].each {|file| require file }

