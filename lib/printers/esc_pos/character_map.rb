module Printers::EscPos
  module CharacterMap
    FF = 12
    LF = 10
    CR = 13
    HT = 9
    DLE = 16
    CAN = 24
    ESC = 27
    FS = 28
    GS = 29
    SPACE = 32
  end

  module HtmlCharacterMap
    HTML_SPACE = "&nbsp;"
  end
end

include Printers::EscPos::HtmlCharacterMap
include Printers::EscPos::CharacterMap
