module Printers::EscPos::Commands::Fs
  class DefineNvBitImage < Printers::EscPos::Commands::Base
    CODE = 'q'

    def process(pointer)
      n = pointer.shift
      chars = []
      n.times do
        x_l = pointer.shift
        x_h = pointer.shift
        y_l = pointer.shift
        y_h = pointer.shift
        k = (x_l + x_h * 256) * (y_l + y_h * 256) * 8
        char = []
        k.each do
          char << pointer.shift
        end

        chars << char
      end

      get_output_command.try(:process, chars)
    end
  end
end

