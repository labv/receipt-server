module Printers::EscPos::Commands::Fs
  class WriteNvUserMemory < Printers::EscPos::Commands::Base
    CODE = 'g1'

    def process(pointer)
      _a1 = pointer.shift
      _a2 = pointer.shift
      _a3 = pointer.shift
      _a4 = pointer.shift
      n_l = pointer.shift
      n_h = pointer.shift

      k = n_l + n_h * 256
      data = []

      k.times do
        data << pointer.shift
      end

      get_output_command.try(:process, data)
    end
  end
end

