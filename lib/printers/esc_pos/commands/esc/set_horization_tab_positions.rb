module Printers::EscPos::Commands::Esc
  class SetHorizationTabPositions < Printers::EscPos::Commands::Base
    CODE = 'D'

    def process(pointer)
      stream = []

      byte = pointer.shift

      while !byte.nil?
        stream << byte
        byte = pointer.shift
      end

      get_output_command.try(:process, stream)
    end
  end
end

