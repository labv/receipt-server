module Printers::EscPos::Commands::Esc
  class DefineUserDefChars < Printers::EscPos::Commands::Base
    CODE = '&'

    def process(pointer)
      characters = []
      y = pointer.shift
      c1 = pointer.shift
      c2 = pointer.shift
      k = c2 - c1 + 1

      k.times do
        character = []
        x = pointer.shift
        dot_length = x * y
        dot_length.times do
          character << pointer.shift
        end

        characters << character
      end

      get_output_command.try(:process, characters)
    end
  end
end

