module Printers::EscPos::Commands::Esc
  class SelectBitImageMode < Printers::EscPos::Commands::Base
    CODE = '*'

    def process(pointer)
      m = pointer.shift
      n_l = pointer.shift
      n_h = pointer.shift

      bytes_per_column = case m
                         when 0, 1
                           1
                         when 32, 33
                           3
                         else
                           raise "Invalid m #{m}"
                         end

      k = bytes_per_column * (n_l + n_h * 256)
      width = k * 8
      height = m * 8

      stream = []
      k.times do
        stream << pointer.shift
      end

      x_multiplier = 1
      y_multiplier = 1

      # Vertical format. Treat as horizontal than transpose
      bit_array = Printers::EscPos::ImageConverter.new.
        convert(stream, height, width, y_multiplier, x_multiplier).transpose

      get_output_command.try(:process, bit_array, width, height)
    end
  end
end

