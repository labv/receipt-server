module Printers::EscPos::Commands
  module FixedLengthCommands
    # Build classes from the defined hash. Metaprogramming
    def self.build(logger = Rails.logger)
      # Build non-variable length commands
      COMMANDS.each do |type, cmds|
        module_name = "Printers::EscPos::Commands::#{type.to_s.capitalize}"
        mod = module_name.constantize

        cmds.each do |code, (name, args)|
          # Convert string to symbol
          name = name.gsub(/ /, '_').downcase.to_sym if name.is_a? String

          # Metaprogramming, build class
          klass = Class.new(Printers::EscPos::Commands::Base) do
            # Just read the fixed # of bytes
            define_method(:process) do |pointer|
              params = args.times.map do
                pointer.shift
              end

              logger.debug "#{name} stub"

              # Call output handler (if implemented)
              get_output_command.try(:process, *params)
            end
          end

          # Setup the search code
          klass.const_set :CODE, code

          # Assign name
          mod.const_set name.to_s.classify, klass
        end
      end

      # This refers to commands with ( in the code
      # this class of commands may have variable length but the length is well defined
      COMMON_VAR_ARGS.each do |type, cmds|
        module_name = "Printers::EscPos::Commands::#{type.to_s.capitalize}"
        mod = module_name.constantize

        cmds.each do |code, name|
          name = name.gsub(/ /, '_').downcase.to_sym if name.is_a? String
          klass = Class.new(Printers::EscPos::Commands::Base) do
            define_method(:process) do |pointer|
              p_l = pointer.shift
              p_h = pointer.shift
              # pL and pH defines the # of byte follows
              params = []
              (p_l + p_h * 256).times do
                params << pointer.shift
              end
              get_output_command.try(:process, *params)
            end
          end

          logger.debug "#{name} stub"

          klass.const_set :CODE, code
          mod.const_set name.to_s.classify, klass
        end
      end
    end

    private

    COMMANDS = {
      :esc => {
        "\x0C" => [:ff, 0],
        "\x20" => [:right_side_spacing, 1],
        "!"    => [:select_print_mode, 1],
        "$"    => [:set_abs_print_pos, 2],
        "%"    => [:toggle_user_def_char_set, 1],
        "-"    => [:toggle_underline_mode, 1],
        "2"    => [:default_line_height, 0],
        "3"    => [:set_default_line_height, 1],
        "<"    => [:return_home, 0],
        "="    => [:select_peripheral_device, 1],
        "?"    => [:cancel_user_defined_chars, 1],
        "@"    => [:initialize, 0],
        "E"    => [:set_emphasized, 1],
        "G"    => [:set_double_strike, 1],
        "J"    => [:print_and_feed, 1],
        "K"    => [:print_and_reverse_feed, 1],
        "K"    => [:print_and_reverse_feed, 1],
        "L"    => [:select_page_mode, 0],
        "M"    => [:set_font, 1],
        "R"    => [:select_internation_char_set, 1],
        "S"    => [:select_standard_mode, 0],
        "T"    => [:select_print_direction, 1],
        "U"    => [:set_unidirectional_print_mode, 1],
        "V"    => [:clockwise_rotate_90, 1],
        "W"    => [:set_print_area, 8],
        "\\"   => [:set_relative_print_pos, 2],
        "a"    => [:justification, 1],
        "c0"   => [:paper_sensor_unknown_1, 1],
        "c1"   => [:paper_sensor_unknown_2, 1],
        "c3"   => [:paper_sensor_for_paper_end_signal, 1],
        "c4"   => [:paper_sensor_to_stop_printing, 1],
        "c5"   => [:toggle_panel_buttons, 1],
        "d"    => [:print_and_feed_line, 1],
        "e"    => [:print_and_reverse_feed_line, 1],
        "i"    => [:partial_cut_one_point_left, 0],
        "m"    => [:partial_cut_three_point_left, 0],
        "p"    => [:generate_pulse, 3],
        "r"    => [:printer_color, 1],
        "t"    => [:select_char_code_table, 1],
        "u"    => [:transmit_peripheral_device_status, 0],
        "v"    => [:transmit_paper_sensor_status, 0],
        "{"    => [:toggle_upsidedown_print_mode, 1],
      },
      :fs => {
        "!"    => [:select_print_mode_kanji_char, 1],
        "&"    => [:select_kanji_character_mode, 1],
#        "(A\x02\x00\x30"    => [:select_kanji_character_font, 1],
#        "(E\x06\x00\x3C"    => [:cancel_values_logo_printing, 5],
#        "(E\x03\x00\x3D"    => [:trasmit_values_logo_printing, 2],
#        "(E\x06\x00\x3E"    => [:set_top_logo_printing, 5],
#        "(E\x05\x00\x3F"    => [:set_bottom_logo_printing, 4],
#        "(E\x04\x00\x41"    => [:toggle_top_bottom_logo_printing, 3],
#        "(L\x02\x00\x22"    => [:paper_layout_info_transm, 1],
#        "(L\x02\x00\x30"    => [:transmit_pos_info, 1],
#        "(L\x02\x00\x41"    => [:feed_paper_label_peeling_pos, 1],
#        "(L\x02\x00\x42"    => [:feed_paper_cut_pos, 1],
#        "(L\x02\x00\x43"    => [:feed_paper_print_start_pos, 1],
#        "(L\x02\x00\x50"    => [:paper_layout_error_margin, 1],
#        "(e"                => [:toggle_asb, 4],
        "-"                 => [:underline_kanji_char, 1],
        "."                 => [:cancel_kanji_char_mode, 0],
        "?"                 => [:cancel_user_def_kanji_chars, 2],
        "C"                 => [:select_kanji_char_code, 1],
        "S"                 => [:set_kanji_char_spacing, 2],
        "W"                 => [:quadruple_size_kanji, 1],
        "g2"                => [:read_nv_user_memory, 7],
        "p"                 => [:print_nv_bit_image, 2],
      },
      :gs => {
        "!"                 => [:select_char_size, 1],
        "$"                 => [:set_abs_vert_print_pos, 2],
#        "(A"                => [:execute_test_print, 4],
#        "(C\x05\x00\x00\x00"=> [:delete_specific_record, 3],
#        "(C\x05\x00\x00\x30"=> [:delete_specific_record2, 3],
#        "(C\x05\x00\x00\x02"=> [:transmit_record_data, 3],
#        "(C\x05\x00\x00\x32"=> [:transmit_record_data2, 3],
#        "(C\x03\x00\x00\x03"=> [:transmit_nv_memory_used, 1],
#        "(C\x03\x00\x00\x33"=> [:transmit_nv_memory_used, 1],
#        "(C\x03\x00\x00\x04"=> [:transmit_nv_memory_remaining, 1],
#        "(C\x03\x00\x00\x34"=> [:transmit_nv_memory_remaining, 1],
#        "(C\x03\x00\x00\x05"=> [:transmit_key_code_list, 1],
#        "(C\x03\x00\x00\x35"=> [:transmit_key_code_list2, 1],
#        "(C\x06\x00\x00\x06"=> [:delete_nv_memory, 4],
#        "(C\x06\x00\x00\x36"=> [:delete_nv_memory2, 4],
#        "(E\x03\x00\x01"    => [:change_into_user_setting_mode, 2],
#        "(E\x04\x00\x02"    => [:end_user_setting_mode, 3],
#        "(E\x02\x00\x04"    => [:transmit_setting_memory_switch, 1],
#        "(E\x02\x00\x06"    => [:transmit_customized_settings, 1],
#        "(E\x04\x00\x07"    => [:copy_user_def_page, 3],
#        "(E\x03\x00\x0A"    => [:del_data_char_code_page, 2],
#        "(E\x02\x00\x0C"    => [:transmit_conf_item_for_serial, 1],
#        "(E\x02\x00\x0E"    => [:transmit_conf_item_for_bluetooth, 1],
#        "(E\x03\x00\x0F"    => [:transmit_conf_item_for_usb, 2],
#        "(E\x02\x00\x10"    => [:transmit_condition_for_usb, 1],
#        "(E\x04\x00\x30"    => [:delete_paper_layout, 3],
#        "(E\x02\x00\x32"    => [:transmit_paper_layout_info, 1],
#        "(E\x02\x00\x64"    => [:transmit_internal_buzzer_pattern, 1],
#        "(H\x06\x00\x30"    => [:set_process_id_response, 5],
#        "(H\x03\x00\x31"    => [:set_offline_response, 2],
#        "(K\x02\x00\x30"    => [:select_print_control_mode, 1],
#        "(K\x02\x00\x31"    => [:select_print_density, 1],
#        "(K\x02\x00\x32"    => [:select_print_speed, 1],
#        "(K\x02\x00\x61"    => [:select_num_of_parts_thermal_head, 1],
#        "(L\x02\x00\x30"    => [:transmit_nv_graphics_memory_capacity, 1],
#        "(L\x02\x00\x30\x00"    => [:transmit_nv_graphics_memory_capacity, 0],
#        "(L\x02\x00\x30\x30"    => [:transmit_nv_graphics_memory_capacity2, 0],
#        "(L\x04\x00\x30\x01"    => [:set_ref_dot_density_for_graphics, 2],
#        "(L\x04\x00\x30\x31"    => [:set_ref_dot_density_for_graphics2, 2],
#        "(L\x02\x00\x30\x02"    => [:print_graphic_data_print_buffer, 0],
#        "(L\x02\x00\x30\x32"    => [:print_graphic_data_print_buffer2, 0],
#        "(L\x02\x00\x30\x03"    => [:transmit_remaining_capacity_nv_graphics_memory, 0],
#        "(L\x02\x00\x30\x33"    => [:transmit_remaining_capacity_nv_graphics_memory2, 0],
#        "(L\x02\x00\x30\x04"    => [:transmit_remaining_capacity_download_graphics_memory, 0],
#        "(L\x02\x00\x30\x34"    => [:transmit_remaining_capacity_download_graphics_memory2, 0],
#        "(L\x04\x00\x30\x40"    => [:transmit_key_code_list_nv_graphics, 2],
#        "(L\x05\x00\x30\x41"    => [:delete_all_nv_graphics, 3],
#        "(L\x04\x00\x30\x42"    => [:delete_specified_nv_graphics, 2],
#        "(L\x06\x00\x30\x45"    => [:print_specified_nv_graphics_data, 4],
#        "(L\x04\x00\x30\x50"    => [:transmit_key_code_def_download_graphics, 2],
#        "(L\x05\x00\x30\x51"    => [:delete_all_download_graphic_data, 3],
#        "(L\x04\x00\x30\x52"    => [:delete_specified_download_graphic_data, 2],
#        "(L\x06\x00\x30\x55"    => [:print_specified_download_graphic_data, 4],
#
#        "(M\x02\x00\x01"    => [:save_setting_values_from_work_area_into_storage_area, 1],
#        "(M\x02\x00\x31"    => [:save_setting_values_from_work_area_into_storage_area2, 1],
#        "(M\x02\x00\x02"    => [:load_setting_values_in_storage_into_work, 1],
#        "(M\x02\x00\x32"    => [:load_setting_values_in_storage_into_work2, 1],
#        "(M\x02\x00\x03"    => [:set_setting_values_in_work_area, 1],
#        "(M\x02\x00\x33"    => [:set_setting_values_in_work_area, 1],
#
#        "(N\x02\x00\x30"    => [:select_char_code, 1],
#        "(N\x02\x00\x31"    => [:select_background_code, 1],
#        "(N\x03\x00\x32"    => [:turn_shading_on_off, 2],
#
#        "(P\x08\x00\x30"    => [:set_printable_area_in_page, 7],
#
#        "(Q\x0C\x00\x30"    => [:draw_line, 11],
#        "(Q\x0E\x00\x31"    => [:draw_rectangle, 13],

        "/"                 => ["Print downloaded bit image", 1],
        ":"                 => [:start_end_macro_def, 0],
        "B"                 => [:turn_bw_reverse_print_mode_on_off, 1],

        "C0"                => ["Select counter print mode", 2],
        "C1"                => ["Select count mode A", 6],
        "C2"                => ["set counter", 2],
        "C;"                => ["select count mode B", 10],
        "C;"                => ["select count mode B", 10],

        "H"                 => ["Select print position of HRI characters", 1],
        "I"                 => [:transmit_printer_id, 1],
        "L"                 => [:set_left_margin, 2],
        "P"                 => [:set_hor_vert_motion_units, 2],
        "T"                 => [:set_print_pos_to_start_of_print_line, 1],
        "VA"                 => [:select_cut_mode_and_cut_unknown, 0],
        "V\x00"                 => [:select_cut_mode_and_cut_a_full, 0],
        "V\x48"                 => [:select_cut_mode_and_cut_a_2_full, 0],
        "V\x01"                 => [:select_cut_mode_and_cut_a_partial, 0],
        "V\x49"                 => [:select_cut_mode_and_cut_a_partial_2, 0],
        "V\x65"                 => [:select_cut_mode_and_cut_b_full, 1],
        "V\x66"                 => [:select_cut_mode_and_cut_b_partial, 1],
        "V\x97"                 => [:select_cut_mode_and_cut_c_full, 1],
        "V\x98"                 => [:select_cut_mode_and_cut_c_partial, 1],
        "V\x103"                => [:select_cut_mode_and_cut_d_full, 1],
        "V\x104"                => [:select_cut_mode_and_cut_d_partial, 1],
        "W"                => [:set_print_area_width, 2],
        "\\"               => ["Set relative vertical print position in Page mode", 2],
        "^"                => [:exec_macro, 3],
        "a"                => [:asb, 1],
        "b"                => [:smooth_mode, 1],
        "c"                => [:print_count, 0],
        "f"                => ["Select font for HRI characters", 1],
        "g\x00"            => ["Initialize maintenance counter", 2],
        "g\x02"            => ["Transmit maintenance counter", 2],
        "h"            => [:set_barcode_height, 1],
        "j"            => [:asb_for_ink, 1],
        "r"            => [:transmit_status, 1],
        "w"            => [:set_barcode_width, 1],
        "z0"            => [:set_online_recovery_wait_time, 2],
      },
      :dle => {
        "\x04\x01"     => [:transmit_printer_status, 0],
        "\x04\x02"     => [:transmit_offline_cause_status, 0],
        "\x04\x03"     => [:transmit_error_cause_status, 0],
        "\x04\x04"     => [:transmit_roll_paper_sensor_status, 0],
        "\x04\x07"     => [:transmit_ink_status_a, 1],
        "\x04\x08"     => [:transmit_peeler_status, 1],
        "\x05"     => [:send_real_time_request_to_printer, 1],
        "\x05"     => [:send_real_time_request_to_printer, 1],
        "\x14\x01"     => [:generate_pulse_in_real_time, 2],
        "\x14\x02\x01\x08"     => [:execute_power_off_sequence, 0],
        "\x14\x03"     => [:sound_buzzer_in_real_time, 5],
        "\x14\x07"     => [:transmit_specified_status_in_real_time, 1],
        "\x14\x08"     => [:clear_buffers, 7],
      }
    }

    # TODO IMPLEMENT THESE
    VAR_COMMANDS = {
      :fs => {
        "2"                 => [:def_user_kanji_chars, 0],
      },
      :gs => {
        "D;"                => ["select count mode B", 10], # D67
        "D;"                => ["select count mode B", 10], # D83
      }
    }

    COMMON_VAR_ARGS = {
      :esc => {
        "(" => :esc_common_var_args
      },
      :fs => {
        "(" => :fs_common_var_args,
      },
      :gs => {
        "(" => :gs_common_var_args,
      }
    }

    build
  end
end

