module Printers::EscPos::Commands
  class Base
    def initialize(output_format, outputter_params = {})
      @output_format = output_format.to_s
      @outputter_params = outputter_params
    end

    protected

    def get_output_command
      klass_name =
        "Printers::EscPos::" \
        "#{@output_format.titleize}::" \
        "#{self.class.name.split('::')[-2..-1].join('::')}"

      klass_name.constantize.new(@outputter_params)
    rescue
      return nil
    end
  end
end

