module Printers::EscPos::Commands::Gs
  class DefineDownloadedBitImage < Printers::EscPos::Commands::Base
    CODE = '*'

    def process(pointer)
      x = pointer.shift
      y = pointer.shift
      k = x * y * 8

      data = []

      k.times do
        data << pointer.shift
      end

      get_output_command.try(:process, data)
    end
  end
end

