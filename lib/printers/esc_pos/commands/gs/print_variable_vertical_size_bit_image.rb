module Printers::EscPos::Commands::Gs
  class PrintVariableVerticalSizeBitImage < Printers::EscPos::Commands::Base
    CODE = 'Q0'

    def process(pointer)
      _m = pointer.shift
      x_l = pointer.shift
      x_h = pointer.shift
      y_l = pointer.shift
      y_h = pointer.shift

      stream = []

      ((x_l + x_h * 256) * (y_l + y_h * 256)).times do
        stream << pointer.shift
      end

      get_output_command.try(:process, stream)
    end
  end
end

