module Printers::EscPos::Commands::Gs
  class PrintBarcode < Printers::EscPos::Commands::Base
    CODE = 'k'

    def process(pointer)
      m = pointer.shift

      data = []

      if m < 65
        # function A
        # NULL terminated
        byte = pointer.shift
        while byte != 0
          data << byte
          byte = pointer.shift
        end
      else
        # function B
        # Defined lenght
        n = pointer.shift
        n.times do
          data << pointer.shift
        end
      end

      get_output_command.try(:process, data)
    end
  end
end

