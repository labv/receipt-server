module Printers::EscPos::Commands::Gs
  class PrintRasterBitImage < Printers::EscPos::Commands::Base
    CODE = 'v0'

    def process(pointer)
      m = pointer.shift
      x_l = pointer.shift
      x_h = pointer.shift
      y_l = pointer.shift
      y_h = pointer.shift

      stream = []

      ((x_l + x_h * 256) * (y_l + y_h * 256)).times do
        stream << pointer.shift
      end

      width = (x_l + x_h * 256) * 8
      height = y_l + y_h * 256

      x_multiplier = 1
      y_multiplier = 1

      case m
      when 0, 48
        x_multiplier *= 1
        y_multiplier *= 1
      when 1, 49
        x_multiplier *= 2
        y_multiplier *= 1
      when 2, 50
        x_multiplier *= 1
        y_multiplier *= 2
      when 3, 51
        x_multiplier *= 2
        y_multiplier *= 2
      else
        logger.error "Invalid m #{m}"
      end

      bit_array = Printers::EscPos::ImageConverter.new.convert(stream, width, height, x_multiplier, y_multiplier)

      get_output_command.try(:process, bit_array, width, height)
    end
  end
end

