module Printers::EscPos::Text
  module BaseModule
    class DoNothing < Printers::EscPos::Text::BaseCommand
      ARG_LENGTH = 0
      def process(*args)
      end
    end

    def to_processor(code)
      @map ||= _build_map
      klass = @map[code]
      klass || DoNothing
    end

    private

    def _build_map
      classes = self.constants.map do |c|
        self.const_get(c)
      end.select do |c|
        c.is_a?(Class) && c.constants.include?(:CODE)
      end

      Hash[
        classes.map do |c|
          [c.const_get(:CODE).ord, c]
        end
      ]
    end
  end
end

