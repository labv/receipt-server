module Printers::EscPos::Text
  class BaseCommand
    # @param [TextBuffer] text buffer. Store char by char unflushed text buffer
    # @param [StringIO] text output. Output text
    def initialize(options = {})
      @text_buffer = options[:text_buffer]
      @text_output = options[:text_output]
      @logger = options[:logger] || Rails.logger
    end

    def process(*args)
      raise 'Not implemented'
    end

    protected

    def _print_buffer
      @text_output << @text_buffer.to_s + "\n"
      @text_buffer.clear
    end
  end
end

