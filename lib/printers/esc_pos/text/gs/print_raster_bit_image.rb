module Printers::EscPos::Text::Gs
  class PrintRasterBitImage < Printers::EscPos::Text::BaseCommand

    def process(bit_array, width, height)
      image = Printers::EscPos::ImageConverter.to_magick(bit_array, width, height, false)
      t = RTesseract.new(image, :options => ['-psm 6'])
      @text_output << t.to_s
    end
  end
end

