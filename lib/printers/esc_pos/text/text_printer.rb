module Printers::EscPos::Text
  class TextPrinter
    include Printers::EscPos::CharacterMap

    BASE_PATH = '/var/www/files/'

    def initialize(logger = nil)
      @logger = logger || Rails.logger
    end

    def convert_as_file(raw_content, path = nil, filename = nil)
      dir = "#{BASE_PATH || path}#{Date.today.strftime("%Y%m%d")}/"
      Dir.mkdir(dir) unless Dir.exist?(dir)

      file_path = "#{dir}#{filename || SecureRandom.uuid.gsub(/-/, '')}.txt"

      File.write(file_path, convert(raw_content))

      file_path
    end

    def convert(raw_content)
      _convert(raw_content)
    end

    private

    def _convert(raw_content)
      output = StringIO.new
      content = raw_content.each_byte.to_a
      text_buffer = Printers::EscPos::TextBuffer.new

      begin
        byte = content.shift

        while(byte != nil)
          klass = nil
          line_feed = PrintAndFeedLine.new(
            :text_buffer => text_buffer,
            :text_output => output,
            :logger => @logger
          )

          case byte
          when FF, LF, CR
            line_feed.process
          when ESC
            klass = Printers::EscPos::Commands.to_processor(:esc, content)
          when GS
            klass = Printers::EscPos::Commands.to_processor(:gs, content)
          when DLE
            klass = Printers::EscPos::Commands.to_processor(:dle, content)
          when CAN
            klass = Printers::EscPos::Commands.to_processor(:can, content)
          when FS
            klass = Printers::EscPos::Commands.to_processor(:fs, content)
          when HT
            text_buffer << "\t"
          when SPACE
            text_buffer << ' '
          when '\x00'
          else
            text_buffer << byte
          end

          if klass.present?
            handler = klass.new(
              :text,
              :text_buffer => text_buffer,
              :text_output => output,
              :logger => @logger
            )

            handler.process(content)
          end

          byte = content.shift
        end
      end

      output.string
    end
  end
end

