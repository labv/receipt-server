module Printers::EscPos::Text
  class PrintAndFeedLine < BaseCommand
    ARG_LENGTH = 0

    def process(*args)
      _print_buffer
    end
  end
end

