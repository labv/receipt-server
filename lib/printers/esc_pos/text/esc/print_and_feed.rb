module Printers::EscPos::Text::Esc
  class PrintAndFeed < Printers::EscPos::Text::BaseCommand

    def process(*args)
      _print_buffer
    end
  end
end

