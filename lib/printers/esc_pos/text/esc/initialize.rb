module Printers::EscPos::Text::Esc
  class Initialize < Printers::EscPos::Text::BaseCommand

    def process(*args)
      @text_buffer.clear
    end
  end
end

