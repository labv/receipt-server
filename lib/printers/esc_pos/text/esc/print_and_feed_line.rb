module Printers::EscPos::Text::Esc
  class PrintAndFeedLine < Printers::EscPos::Text::BaseCommand

    def process(*args)
      value = args.first || 1

      _print_buffer
      (value - 1).times do
        _print_buffer
      end
    end
  end
end

