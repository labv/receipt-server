module Printers::EscPos
  module Text
  end
end

require_relative('text/base_command')
require_relative('text/base_module')
require_relative('text/esc')
require_relative('text/gs')
require_relative('text/text_printer')
require_relative('text/print_and_feed_line')

Dir[File.dirname(__FILE__) + '/text/gs/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/text/esc/*.rb'].each {|file| require file }

