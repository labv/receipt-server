module Push
  module Gcm
    @@gcm = GCM.new GcmApiKey

    def self.push(tokens, notification, data)
      tokens = [tokens].flatten.compact

      body = {
        :notification => notification.to_hash,
        :data => data.to_hash
      }

      response = @@gcm.send(tokens, body)
      # TODO handle response
    end
  end
end

include Push::Gcm

