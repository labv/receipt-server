module Push
  module Pokes
    ReceiptChanges = ApiSpecStruct.create(:t => { :default => 'r' })
    UserChanges = ApiSpecStruct.create(:t => { :default => 'u' })
    RetailerChanges = ApiSpecStruct.create(:t => { :default => 'm' })
  end
end

