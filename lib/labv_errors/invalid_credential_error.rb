module LabvErrors
  class InvalidCredentialError < LabvArgumentError
    def initialize
      super('')
    end
  end
end
