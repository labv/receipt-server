module LabvErrors
  class InvalidSessionError < LabvArgumentError
    STATUS_CODE = :unauthorized

    def initialize
      super('Invalid session info')
    end
  end
end
