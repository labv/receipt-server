module LabvErrors
  class RetailerNameInUseError < LabvArgumentError
    def initialize
      super('Retailer name in use')
    end
  end
end
