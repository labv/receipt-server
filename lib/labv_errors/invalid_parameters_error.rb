module LabvErrors
  class InvalidParametersError < LabvArgumentError
    def initialize(errors)
      super(errors.is_a?(String) ? errors : "invalid fields: #{errors.keys.join(', ')}")
      @errors = errors
      @params = errors.keys if errors.is_a? Hash
    end
  end
end
