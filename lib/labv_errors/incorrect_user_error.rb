module LabvErrors
  class IncorrectUserError < LabvArgumentError
    STATUS_CODE = :forbidden

    def initialize
      super('Cannot operate on other users')
    end
  end
end
