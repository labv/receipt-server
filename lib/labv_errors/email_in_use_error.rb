module LabvErrors
  class EmailInUseError < LabvArgumentError
    def initialize
      super('Email in use')
    end
  end
end
