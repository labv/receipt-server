module LabvErrors
  class ObjectNotFoundError < LabvBaseError
    STATUS_CODE = :not_found

    def initialize(mongo_error)
      super("Cannot find #{mongo_error.klass} with #{mongo_error.params.inspect}")
      @error = mongo_error
    end
  end
end
