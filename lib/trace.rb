module Trace
  def self.included(base)
    base.class_eval do
      extend ClassMethods
    end
  end

  module ClassMethods
    attr_reader :before_hooks

    def trace(method_name)
      untrace_method_name = "__#{method_name}"
      alias_method untrace_method_name, method_name

      define_method(method_name) do |*args|
        startTime = Time.now
        if self.class.instance_method(untrace_method_name).arity == 0
          self.send(untrace_method_name)
        else
          self.send(untrace_method_name, *args)
        end
        endTime = Time.now
        puts "#{method_name}: #{endTime - startTime}"
      end
    end
  end

  def self.trace(name)
    startTime = Time.now
    yield
    endTime = Time.now
    puts "#{name}: #{endTime - startTime}"
  end
end


