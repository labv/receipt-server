require 'logging'

class RequestLogger
  def initialize(app, options={})
    @app = app
    @options = options
  end

  def call(env)
    status, headers, response = @app.call(env)
    request = Rack::Request.new(env)

    id = nil
    id = env['action_dispatch.request_id'] unless env['action_dispatch.request_id'].blank?
    id = headers['X-Request-Id'] unless headers['X-Request-Id'].blank?

    request_body = nil
    if env['rack.input']
      request_body = env['rack.input'].read
      env['rack.input'].rewind
      begin
        request_body = ActiveSupport::JSON.decode(request_body) if request_body.is_a? String
      rescue
      end
    end

    response_body = nil
    if ((!@options[:environment].blank? && @options[:environment] == 'development') || (status / 100).to_i != 200) && response.respond_to?(:body)
      begin
        response_body = response.body unless response.body.blank?
        response_body = ActiveSupport::JSON.decode(response_body) if response_body.is_a? String
      rescue
      end
    end

    response_status = response.respond_to?(:status) ? response.status : nil

    Logging.logger['requests'].info(
      {
        id: id,
        method: env['REQUEST_METHOD'],
        path: env['REQUEST_PATH'],
        params: request_body,
        response: {
          status_code: response_status,
          body: response_body
        }
      }
    )

    [status, headers, response]
  end
end

