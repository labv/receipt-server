class Hash
  # Convert hash to struct
  #
  # @param struct [Class] Struct clas
  # @return [Object<Class>]
  def to_struct(struct)
    extra_keys = (self.keys.map {|k| k.to_sym} -
                  struct.members.map{|m| m.to_sym})
    if extra_keys.present?
      raise "Hash has key(s) unsupported by #{struct.to_s}: #{extra_keys.inspect}"
    end

    struct.new(*self.values_at(*struct.members))
  end
end
