module LabvTypes
  module Helpers
    def self.to_hash
      Hash[self.constants.map do |constant|
        Hash[constant, self.const_get(constant)]
      end]
    end

    def self.members
      self.to_hash.keys
    end

    def self.values
      self.to_hash.values
    end
  end

  module Gender
    MALE = 1
    FEMALE = 2
  end


  # Inject Helper module
  LabvTypes.constants.map do |x|
    LabvTypes.const_get(x)
  end.select do |x|
    x.is_a?(Module) && x != LabvTypes::Helpers
  end.each do |x|
    x.send(:include, LabvTypes::Helpers)
  end
end
