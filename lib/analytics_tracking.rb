module AnalyticsTracking
  module EventTypes
    EMAIL_VERIFICATION = :ev
    RESET_PASSWORD = :reset
  end

  WEB = 'web'

  def self.build_tracking_options(type, uidh, options = {})
    {
      :e => type,
      :uidh => uidh,
      :source => WEB
    }.merge(options)
  end
end

