class ApiSpecStruct
  private_class_method :new

  # Factory method to create a new struct with presence validation
  #
  # The resulting struct includes a `valid?` and an `errors` methods
  #   for validations.
  #
  # Examples:
  #
  # Required `interests` field:
  #  Spec = ApiSpecStruct.create(:interests => { :required => true })
  #  Spec.new(nil).valid?       # False
  #  Spec.new(nil).errors       # {:interests => 'missing'}
  #
  # Shorthand for required `interests` field:
  #  Spec = ApiSpecStruct.create(:interests => true)
  #  Spec.new(nil).valid?       # False
  #  Spec.new(nil).errors       # {:interests => 'missing'}
  #
  # Optional `interests` field:
  #  Spec = ApiSpecStruct.create(:interests => false)
  #  Spec.new(nil).valid?       # True
  #  Spec.new(nil).errors       # {}
  #
  # Default `interests` field to be optional:
  #  Spec = ApiSpecStruct.create(:interests)
  #  Spec.new(nil).valid?       # True
  #  Spec.new(nil).errors       # {}
  #
  # Multiple-mixture of fields:
  #  Spec = ApiSpecStruct.create(:interests => false, :gender => true)
  #  Spec.new(nil, nil).valid?  # False
  #  Spec.new(nil, nil).errors  # {:gender => 'missing'}
  #
  # Default value:
  #  Spec = ApiSpecStruct.create(:interests => { :default => 1 })
  #  Spec.new(nil).valid?       # True
  #  Spec.new(nil).errors       # {}
  #  Spec.new(nil).interests    # 1
  #
  # Numericality validation:
  #  Spec = ApiSpecStruct.create(:interests => { :numericality => true })
  #  Spec.new(nil).valid?       # False
  #  Spec.new(nil).errors       # {:numericality => "is not numeric."}
  #
  #  Spec.new("test").valid?    # False
  #  Spec.new("test").errors    # {:numericality => " test is not numeric."}
  #
  # Numericality validation that allows nil:
  #  Spec = ApiSpecStruct.create(:interests =>
  #                                 { :numericality => { :allow_nil => true } }
  #                             )
  #  Spec.new(nil).valid?       # true
  #  Spec.new(nil).errors       # {}

  # @params args [Array] Variable argument
  # @return [Struct] Struct with 'valid?' and 'errors' methods injected
  def self.create(*args, &block)
    fields = args.reduce({}) do |memo, a|
      # each argument can either be a hash or a symbol or string
      if a.is_a? Hash
        a.each do |f, options|
          # option used to accept a boolean (required)
          if options == true || options == false
            options = {
              :required => options
            }
          end

          options[:required] ||= false
          memo[f] = options
        end
      elsif a.is_a?(Symbol) || a.is_a?(String)
        memo[a.to_sym] = {:required => false}
      end

      memo
    end

    field_options = {}
    fields.each do |field, options|
      field_options[field] = {
        :validators => options.select do |option, option_params|
          ApiSpecStruct._classify_option(option) <= Options::Validator
        end,
        :options => options.select do |option, option_params|
          begin
            !(ApiSpecStruct._classify_option(option) <= Options::Validator)
          rescue
          end
        end
      }
    end

    r = Struct.new(*fields.keys) do
      # Define initalizer to process options
      define_method(:initialize) do |*args|
        super(*args)
        field_options.each do |field, options|
          self[field] = _run_options(field,
                                     options[:options],
                                     self[field])
        end
      end

      # factory method from hash
      define_singleton_method(:from_hash) do |hash|
        hash.to_struct(self)
      end

      define_method(:errors) do
        errors = {}
        field_options.each do |field, options|
          begin
            self[field] = _run_validators(field,
                                          options[:validators],
                                          self[field])
          rescue Errors::ValidationFailedError => e
            errors[field] = e.message
          end
        end

        errors
      end

      define_method(:valid?) do
        if errors.present?
          raise errors.inspect
        end

        true
      end

      block.call if block

      private

      # Run validators
      def _run_validators(field, validators, value)
        # Options is a hash
        validators.each do |(validator, option)|
          validator_class = ApiSpecStruct._classify_option(validator)
          value = validator_class.new(option).process(value)
        end
        value
      end

      # Run options
      def _run_options(field, options, value)
        # Options is a hash
        options.each do |(preprocessor, option)|
          option_class = ApiSpecStruct._classify_option(preprocessor)
          value = option_class.new(option).process(value)
        end

        value
      end
    end

    r
  end

  private

  def self._classify_option(option)
    "ApiSpecStruct::Options::#{option.to_s.classify}".constantize
  rescue
    raise Errors::OptionNotFoundError.new "#{option} is not a valid option"
  end

  module Errors
    class ApiSpecStructError < StandardError
    end

    class OptionNotFoundError < ApiSpecStructError
    end

    class ValidationFailedError < ApiSpecStructError
    end
  end

  # Pre-processors
  module Options
    # Default value pre processor
    class Default
      def initialize(default_value)
        raise "Default value is required" if default_value.nil?
        @default_value = default_value
      end

      def process(value)
        value || @default_value
      end
    end

    # Validator super class
    class Validator
      def process(value)
        value = valid!(value)
        value
      end
    end

    # Validates numericality
    class Numericality < Validator
      OPTIONS_AND_DEFAULTS = {
        :on => true,
        :allow_nil => false,
        :greater_than => nil,
        :less_than => nil,
        :greater_than_or_equals => nil,
        :less_than_or_equals => nil,
        :equals => nil,
      }

      def initialize(options)
        if options == true || options == false
          @options = { :on => options }
        elsif options.is_a? Hash
          @options = OPTIONS_AND_DEFAULTS.merge(options.symbolize_keys)
        else
          raise "Unknown option #{options}"
        end
      end

      def valid!(value)
        if @options[:on] &&
          @options[:allow_nil] &&
          value.nil?
          return
        end

        begin
          Float(value)
        rescue
          raise Errors::ValidationFailedError.new "#{value} is not numeric."
        end

        if value.is_a? String
          value = BigDecimal.new(value)
        end

        if @options[:greater_than].present? &&
          !(value > @options[:greater_than])
          raise Errors::ValidationFailedError.new "#{value} is not greater than #{@options[:greater_than]}."
        end

        if @options[:greater_than_or_equals].present? &&
          !(value >= @options[:greater_than_or_equals])
          raise Errors::ValidationFailedError.new "#{value} is not greater than or equal #{@options[:greater_than_or_equals]}."
        end

        if @options[:less_than].present? &&
          !(value < @options[:less_than])
          raise Errors::ValidationFailedError.new "#{value} is not less than #{@options[:less_than]}."
        end

        if @options[:less_than_or_equals].present? &&
          !(value <= @options[:less_than_or_equals])
          raise Errors::ValidationFailedError.new "#{value} is not less than or equal #{@options[:less_than_or_equals]}."
        end

        if @options[:equals].present? &&
          !(value == @options[:equals])
          raise Errors::ValidationFailedError.new "#{value} does not equal #{@options[:equals]}."
        end

        value
      end
    end

    # Validates required field
    class Required < Validator
      def initialize(required)
        unless required == true || required == false
          raise "Option must be boolean for Required validator"
        end
        @required = required
      end

      def valid!(value)
        if @required && value.nil?
          raise Errors::ValidationFailedError.new "missing."
        end

        value
      end
    end
  end
end


