module ExternalStorageProviders
  class LocalFileSystemProvider
    attr_accessor :base_path

    def initialize(base_path)
      @base_path = base_path
    end

    def get(key)
      return nil unless self.exists?(key)
      File.read(_path(key))
    end

    def exists?(key)
      File.file?(_path(key))
    end

    def put(key, content)
      File.write(_path(key), content)
    end

    def delete(key)
      File.delete(_path(key))
    end

    def path(key)
      _path(key)
    end

    private

    def _path(key)
      "#{@base_path}/#{key}"
    end
  end
end

