module ExternalStorageProviders
  class S3Provider
    MAX_RETRIES = 5

    def initialize(access_key, secret, bucket_name, base_path, logger = nil)
      @access_key = access_key
      @secret = secret
      @bucket_name = bucket_name
      @base_path = base_path
      @logger = logger

      _reset_connection
    end

    # For small pieces data that don't need to be streamed to disk
    def get(s3_key)
      @logger.info("Getting #{s3_key}")
      _with_retry do
        @s3.files.get(_path(s3_key)).try(:body)
      end
    end

    def put(s3_key, content)
      @logger.info("Writing to #{s3_key}")
      _with_retry do
        startTime = Time.now
        @s3.files.create(
          :key => _path(s3_key),
          :body => content
        )
        endTime = Time.now
        @logger.info("Took: #{endTime - startTime}")
      end
    end

    def delete(s3_key)
      _with_retry do
        @s3.files.new(:key => _path(s3_key)).destroy
      end
    end

    def exists?(s3_key)
      _with_retry do
        !@s3.files.get(_path(s3_key)).nil?
      end
    end

    def upload(s3_key, local_path)
      _with_retry do
        @s3.files.create(
          :key => _path(s3_key),
          :body => open(local_path)
        )
      end
    end

    def path(key)
      _path(key)
    end

    private

    def _path(key)
      "#{@base_path}/#{key}"
    end

    def _with_retry(&block)
      attempts = 0

      begin
        attempts += 1

        yield
      rescue Exception, Timeout::Error => e
        if attempts < MAX_RETRIES
          @logger.error(e.message) if @logger
          @logger.info("Retrying attempt #{attempts}") if @logger
          if e.is_a?(EOFError)
            @logger.info("Resetting the S3 connection due to EOFError") if @logger
            _reset_connection
          end

          sleep 2 ** attempts

          retry
        else
          raise e
        end
      end
    end

    def _reset_connection
      @s3 = Fog::Storage.new(:provider              => 'AWS',
                             :aws_access_key_id     => @access_key,
                             :aws_secret_access_key => @secret).
                             directories.get(@bucket_name)
    end
  end
end

