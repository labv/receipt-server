require_relative '../../spec_helper'

class ValidParametersController < ActionController::Metal
  validates :param_1, numericality: { greater_than: 0 }, required: true
  validates :param_2, length: { min: 0, max: 3 }
  validates :param_3, transform: { default: "test" }
  def action
  end

  def all_valid_params
    self.valid_params
  end
end

describe ValidParametersController do
  describe 'adding validators' do
    it 'should configure the correct validations' do
      expect(ValidParametersController.validations.count).to eq(2)
      expect(ValidParametersController.validations[:action].keys).to include('param_1', 'param_2')
    end
  end

  describe 'validates params' do
    it 'should raise errors on invalid params' do
      expect {
        subject.validate_params(:action, { a: '1' })
      }.to raise_error(InvalidParametersError)
    end
  end

  describe 'transform params' do
    it 'should transform param' do
      param = { "param_1" => 10, "param_3" => nil }
      subject.validate_params(:action, param)
      expect(param["param_3"]).to eql("test")
    end
  end

  describe 'valid params global' do
    it 'should set only validated params to valid_param' do
      valid_params = { 'param_1' => 1, 'param_2' => '1', 'param_3' => nil }
      params = valid_params.clone
      params['unwanted'] = 4
      subject.validate_params(:action, params)
      expect(subject.all_valid_params).to eql(valid_params.merge({'param_3' => 'test'}))
    end
  end
end

describe ActionParams::ValidParameters::Transformations::Default do
  subject { ActionParams::ValidParameters::Transformations::Default.new("default") }

  context 'with value' do
    it 'leaves it as is' do
      expect(subject.transform("15195195191")).to eql("15195195191")
    end
  end

  context 'with nil' do
    it 'transforms it to default value' do
      expect(subject.transform(nil)).to eql("default")
    end
  end
end

describe ActionParams::ValidParameters::Transformations::Custom do
  subject { ActionParams::ValidParameters::Transformations::Custom.new(->(x){ x+1 }) }

  it 'transform with lambda' do
    expect(subject.transform(10)).to eql(11)
  end
end

describe ActionParams::ValidParameters::Validations::Numericality do
  subject { ActionParams::ValidParameters::Validations::Numericality.new(greater_than: 0, less_than: 10) }

  context 'with valid number' do
    it 'validates it is a number' do
      expect(subject.valid?(9)).to eq(true)
    end

    it 'validates greater than' do
      expect(subject.valid?(1)).to eq(true)

      expect(subject.valid?(-1)).to eq(false)
      expect(subject.errors.count).to eq(1)
    end

    it 'validates less than' do
      expect(subject.valid?(1)).to eq(true)

      expect(subject.valid?(11)).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end

  context 'with invalid number' do
    it 'errors' do
      expect(subject.valid?('asdf')).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end
end

describe ActionParams::ValidParameters::Validations::Length do
  subject { ActionParams::ValidParameters::Validations::Length.new(min: 3, max: 5) }

  context 'with valid string' do
    it 'validates it is string' do
      expect(subject.valid?('asdf')).to eq(true)
    end

    it 'validates maximum length' do
      expect(subject.valid?('1234')).to eq(true)

      expect(subject.valid?('123456')).to eq(false)
      expect(subject.errors.count).to eq(1)
    end

    it 'validates minimum length' do
      expect(subject.valid?('1234')).to eq(true)

      expect(subject.valid?('12')).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end

  context 'with invalid string' do
    it 'errors' do
      expect(subject.valid?(123123)).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end
end

describe ActionParams::ValidParameters::Validations::Format do
  subject { ActionParams::ValidParameters::Validations::Format.new(with: /^test$/) }

  context 'with valid string' do
    it 'validates it is a string' do
      expect(subject.valid?('test')).to eq(true)
    end

    it 'validates matching regex' do
      expect(subject.valid?('test')).to eq(true)

      expect(subject.valid?('haha')).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end

  context 'with invalid string' do
    it 'errors' do
      expect(subject.valid?(123)).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end
end

describe ActionParams::ValidParameters::Validations::In do
  subject { ActionParams::ValidParameters::Validations::In.new(['a', 'b']) }

  it 'validates in array' do
    expect(subject.valid?('a')).to eq(true)

    expect(subject.valid?('c')).to eq(false)
    expect(subject.errors.count).to eq(1)
  end
end

describe ActionParams::ValidParameters::Validations::Required do
  subject { ActionParams::ValidParameters::Validations::Required.new(true) }

  context 'with param' do
    it 'validates' do
      expect(subject.valid?('something')).to eq(true)
    end
  end

  context 'without param' do
    it 'errors' do
      expect(subject.valid?(nil)).to eq(false)
      expect(subject.errors.count).to eq(1)
    end
  end
end

describe ActionParams::ValidParameters::Validations::Custom do
  subject { ActionParams::ValidParameters::Validations::Custom.new(->(x) {x}) }

  it 'validates' do
    expect(subject.valid?(true)).to eq(true)

    expect(subject.valid?(false)).to eq(false)
    expect(subject.errors.count).to eq(1)
  end
end

describe ActionParams::ValidParameters::Validations::Nested do
  subject { ActionParams::ValidParameters::Validations::Nested.new(a: { required: true }, b: { in: [0, 1] }) }

  it 'validates' do
    expect(subject.valid?({ c: 0 })).to eq(false)

    expect(subject.valid?({ b: '0' })).to eq(false)
    expect(subject.valid?({ a: 'test' })).to eq(true)
    expect(subject.valid?({ a: 'test', b: '0' })).to eq(true)
  end
end

describe ActionParams::ValidParameters::Transformations::Bool do
  subject { ActionParams::ValidParameters::Transformations::Bool.new(nil) }

  it 'validates' do
    expect(subject.transform(1)).to eq(true)
    expect(subject.transform('true')).to eq(true)
    expect(subject.transform('True')).to eq(true)
    expect(subject.transform('yes')).to eq(true)
    expect(subject.transform('Yes')).to eq(true)
    expect(subject.transform('y')).to eq(true)
    expect(subject.transform('Y')).to eq(true)
    expect(subject.transform(true)).to eq(true)
    expect(subject.transform(0)).to eq(false)
    expect(subject.transform(false)).to eq(false)
    expect(subject.transform('false')).to eq(false)
    expect(subject.transform('False')).to eq(false)
    expect(subject.transform('no')).to eq(false)
    expect(subject.transform('No')).to eq(false)
    expect(subject.transform('n')).to eq(false)
    expect(subject.transform('N')).to eq(false)
    expect(subject.transform(nil)).to eq(nil)
    expect(subject.transform('slkdjflkdsf')).to eq(nil)
    expect(subject.transform(1231230)).to eq(nil)
  end
end

describe ActionParams::ValidParameters::Transformations::Date do
  subject { ActionParams::ValidParameters::Transformations::Date.new(nil) }
  it 'validates' do
    expect(subject.transform(nil)).to eq(nil)
    expect(subject.transform('true')).to eq(nil)
    expect(subject.transform('2015-01-01')).to eq(Date.parse('2015-01-01'))
  end
end

describe ActionParams::ValidParameters::Transformations::DateTime do
  subject { ActionParams::ValidParameters::Transformations::DateTime.new(nil) }
  it 'validates' do
    expect(subject.transform(nil)).to eq(nil)
    expect(subject.transform('true')).to eq(nil)
    expect(subject.transform('2015-01-01 14:00:00')).to eq(DateTime.parse('2015-01-01 14:00:00'))
  end
end
