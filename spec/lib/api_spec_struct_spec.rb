require "#{Rails.root}/spec/gma/models/spec_helper.rb"

describe ApiSpecStruct do
  it 'should create basic struct' do
    spec = ApiSpecStruct.create(:interests, :test)
    spec.new(nil).valid?.should be_true       # True
    spec.new(nil).errors.should be_blank       # {}
  end

  it 'should show error if field is required but not supplied' do
    spec = ApiSpecStruct.create(:interests => true, :test => false)
    subject = spec.new(nil, 1)

    expect do
      subject.valid?
    end.to raise_error

    subject.errors.should eql({:interests => 'missing.'})
  end

  it 'should show error if fields are required but not supplied' do
    spec = ApiSpecStruct.create(:interests => true, :test => true)
    subject = spec.new(nil, nil)
    expect do
      subject.valid?
    end.to raise_error
    subject.errors.should eql({:interests => 'missing.', :test => 'missing.'})
  end

  describe 'default' do
    it 'should apply default value' do
      spec = ApiSpecStruct.create(:premium => { :default => 'no' })

      subject = spec.new
      subject.premium.should eql('no')
    end

    it 'should not apply default value if value is given' do
      spec = ApiSpecStruct.create(:premium => { :default => 'no' })

      subject = spec.new('yes')
      subject.premium.should eql('yes')
    end
  end

  describe 'numericality' do
    it 'should validate numericality if value is nil' do
      spec = ApiSpecStruct.create(:premium => { :numericality => true })

      subject = spec.new
      expect do
        subject.valid?
      end.to raise_error
      subject.errors.should eql({:premium => " is not numeric."})
    end

    it 'should validate numericality' do
      spec = ApiSpecStruct.create(:premium => { :numericality => true })

      subject = spec.new("test")
      expect do
        subject.valid?
      end.to raise_error

      subject.errors.should eql({:premium => "test is not numeric."})
    end

    it 'numericality should allow string version of numbers' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => true
      })

      subject = spec.new('10.0')
      subject.valid?.should be_true
      subject.errors.should eql({})
      subject.premium.should eql(10)
    end

    it 'numericality should allow nil if specified' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :allow_nil => true
        }
      })

      subject = spec.new
      subject.valid?.should be_true
      subject.errors.should eql({})
    end

    it 'numericality should fail if not greater than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :greater_than => 1
        }
      })

      subject = spec.new(0.5)
      expect do
        subject.valid?
      end.to raise_error

      subject.errors.should eql({:premium => '0.5 is not greater than 1.'})
    end

    it 'numericality should fail if value equals greater than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :greater_than => 1
        }
      })

      subject = spec.new(1)
      expect do
        subject.valid?
      end.to raise_error
      subject.errors.should eql({:premium => '1 is not greater than 1.'})
    end

    it 'numericality should not fail if fits greater than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :greater_than => 1
        }
      })

      subject = spec.new(2)
      subject.valid?.should be_true
      subject.errors.should eql({})
    end

    it 'numericality should fail if not less than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than => 1
        }
      })

      subject = spec.new(1.5)
      expect do
        subject.valid?
      end.to raise_error

      subject.errors.should eql({:premium => '1.5 is not less than 1.'})
    end

    it 'numericality should fail if value equals less than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than => 1
        }
      })

      subject = spec.new(1)
      expect do
        subject.valid?
      end.to raise_error
      subject.errors.should eql({:premium => '1 is not less than 1.'})
    end

    it 'numericality should not fail if fits less than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than => 1
        }
      })

      subject = spec.new(0.5)
      subject.valid?.should be_true
      subject.errors.should eql({})
    end

    it 'numericality should fail if not less than or equal option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than_or_equals => 1
        }
      })

      subject = spec.new(1.5)
      expect do
        subject.valid?
      end.to raise_error

      subject.errors.should eql({:premium => '1.5 is not less than or equal 1.'})
    end

    it 'numericality should not fail if value equals less than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than_or_equals => 1
        }
      })

      subject = spec.new(1)
      subject.valid?.should be_true
      subject.errors.should eql({})
    end

    it 'numericality should not fail if fits less than option' do
      spec = ApiSpecStruct.create(:premium => {
        :numericality => {
          :less_than_or_equals => 1
        }
      })

      subject = spec.new(0.5)
      subject.valid?.should be_true
      subject.errors.should eql({})
    end
  end
end

