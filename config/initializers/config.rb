config = {}.with_indifferent_access

# Hostnames
config.merge! YAML.load_file('config/hostnames.yml')[Rails.env]

# Set mailer hostname
Rails.application.config.action_mailer.default_url_options = {
  :host => config[:web_host]
}

# External storage
config.merge! YAML.load_file('config/local_external_storage.yml')[Rails.env]

# AWS
aws = YAML.load_file('config/aws.yml')[Rails.env].with_indifferent_access

config[:aws] = OpenStruct.new aws

# SES
ActionMailer::Base.add_delivery_method :ses,
  AWS::SES::Base,
  :access_key_id     => aws[:access_key],
  :secret_access_key => aws[:secret_key]





# Leave this at the last
AppConfig = OpenStruct.new config

