Logging.logger['requests'].add_appenders(
  Logging.appenders.file(
    'log/requests.log',
    :layout => Logging.layouts.pattern(:pattern => '[%d] %m\n')
  )
)
