GcmApiKey = YAML.load_file('config/gcm.yml')[Rails.env].with_indifferent_access[:api_key]

raise "GCM config not found" if GcmApiKey.blank?

