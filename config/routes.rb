ReceiptServer::Application.routes.draw do
  scope :defaults => { :format => 'json' } do
    resources :users, :only => [:index, :create, :show, :destroy, :update] do
      collection do
        get :reset_password_form, :controller => :users, :action => :reset_password_form, :defaults => { :format => :html }
        post :reset_password, :controller => :users, :action => :reset_password, :defaults => { :format => :html }
      end
    end

    resources :sessions, :only => [:create, :destroy, :update]
    delete :sessions, :controller => :sessions, :action => :destroy
    put :sessions, :controller => :sessions, :action => :update
    post 'sessions/reset_password', :controller => :sessions, :action => :reset_password

    resources :receipts, :only => [:index, :create, :show, :destroy, :update]
    resources :retailers, :only => [:show]

    namespace :transceiver do
      resources :receipts, :only => [:create] do
        member do
          post '/receipt_content', :action => :upload_chunk
          get '/receipt_content', :action => :get_chunk
        end
      end
    end

    namespace :admin do
      resources :retailers, :only => [:create, :show, :update, :index, :destroy]
      resources :users, :only => [:index, :create, :show, :destroy, :update]
      resources :receipts, :only => [:index, :create, :show, :destroy, :update]
      resources :transceivers, :only => [:index, :create, :show, :destroy]
    end
  end

  get 'email_verification', :controller => :others, :action => :email_verification
end

