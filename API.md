/users
 GET /users
 Parameters:
  email
   required: true

 POST /users
 Parameters:
  first_name
   required: true
  last_name
   required: true
  platform
   required: true
  email
   required: true
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: true
  password
   required: true
   format: (?-mix:^.{8,}$)

 GET /users/:id

 PATCH /users/:id
 Parameters:
  first_name
   required: false
  last_name
   required: false
  email
   required: false
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: false
  old_password
   required: false
   format: (?-mix:^.{8,}$)
  password
   required: false
   format: (?-mix:^.{8,}$)

 PUT /users/:id
 Parameters:
  first_name
   required: false
  last_name
   required: false
  email
   required: false
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: false
  old_password
   required: false
   format: (?-mix:^.{8,}$)
  password
   required: false
   format: (?-mix:^.{8,}$)

 DELETE /users/:id

 /users/reset_password_form
  GET /users/reset_password_form
  Parameters:
   token
    required: true

 /users/reset_password
  POST /users/reset_password
  Parameters:
   token
    required: true
   new_password
    required: true
   new_password_confirm
    required: true

/sessions
 POST /sessions
 Parameters:
  email
   required: true
  password
   required: true
  platform
   required: true
  device_id
   required: false

 PATCH /sessions/:id
 Parameters:
  push_token
   required: true

 PUT /sessions/:id
 Parameters:
  push_token
   required: true

 DELETE /sessions/:id

 DELETE /sessions

 PUT /sessions
 Parameters:
  push_token
   required: true

 /sessions/reset_password
  POST /sessions/reset_password
  Parameters:
   email
    required: true

/receipts
 GET /receipts
 Parameters:
  page
   required: false
  last_updated_at
   required: false

 POST /receipts
 Parameters:
  user_id
   required: false
  retailer_id
   required: true
  receipt_client_id
   required: true
  receipt_content
   required: true

 GET /receipts/:id

 PATCH /receipts/:id
 Parameters:
  starred
   required: false
  read
   required: false
  user_id
   required: false

 PUT /receipts/:id
 Parameters:
  starred
   required: false
  read
   required: false
  user_id
   required: false

 DELETE /receipts/:id

/retailers
 GET /retailers/:id

/transceiver/receipts
 GET /transceiver/receipts
 Parameters:
  page
   required: false

 POST /transceiver/receipts
 Parameters:
  user_id
   required: false
  receipt_client_id
   required: false
  receipt_content
   required: true

 /transceiver/receipts/receipt_content
  POST /transceiver/receipts/:id/receipt_content
  Parameters:
   receipt_content
    required: true

  GET /transceiver/receipts/:id/receipt_content
  Parameters:
   offset
    required: false
   length
    required: false

/admin/retailers
 GET /admin/retailers
 Parameters:
  name
   required: false
  name_pattern
   required: false

 POST /admin/retailers
 Parameters:
  name
   required: true
  category
   required: false

 GET /admin/retailers/:id

 PATCH /admin/retailers/:id
 Parameters:
  name
   required: false

 PUT /admin/retailers/:id
 Parameters:
  name
   required: false

 DELETE /admin/retailers/:id

/admin/users
 GET /admin/users
 Parameters:
  page
   required: false

 POST /admin/users
 Parameters:
  first_name
   required: true
  last_name
   required: true
  email
   required: true
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: true
  password
   required: true
   format: (?-mix:^.{8,}$)

 GET /admin/users/:id

 PATCH /admin/users/:id
 Parameters:
  first_name
   required: false
  last_name
   required: false
  email
   required: false
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: false
  password
   required: false
   format: (?-mix:^.{8,}$)

 PUT /admin/users/:id
 Parameters:
  first_name
   required: false
  last_name
   required: false
  email
   required: false
   format: (?i-mx:\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z)
  dob
   required: false
  accept_terms
   required: false
  password
   required: false
   format: (?-mix:^.{8,}$)

 DELETE /admin/users/:id

/admin/receipts
 GET /admin/receipts
 Parameters:
  page
   required: false

 POST /admin/receipts
 Parameters:
  user_id
   required: false
  retailer_id
   required: true
  transceiver_id
   required: true
  receipt_client_id
   required: true
  receipt_content
   required: true

 GET /admin/receipts/:id

 PATCH /admin/receipts/:id
 Parameters:
  user_id
   required: true

 PUT /admin/receipts/:id
 Parameters:
  user_id
   required: true

 DELETE /admin/receipts/:id

/admin/transceivers
 GET /admin/transceivers
 Parameters:
  page
   required: false
  retailer_id
   required: false

 POST /admin/transceivers
 Parameters:
  retailer_id
   required: true

 GET /admin/transceivers/:id

 DELETE /admin/transceivers/:id

/email_verification
 GET /email_verification

