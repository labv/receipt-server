json.extract! user,
    :id_s,
    :email,
    :dob,
    :created_at,
    :created_at_i,
    :updated_at,
    :updated_at_i,
    :first_name,
    :last_name
if session.present?
    json.session do
        json.partial! :partial => '/resources/session', :locals => { :session => session }
    end
end

