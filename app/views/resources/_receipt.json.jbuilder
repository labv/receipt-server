namespace = ([request.env['PATH_INFO'].split('/').second] & ['admin', 'transceiver']).first
json.extract! receipt,
    :id_s,
    :created_at,
    :created_at_i,
    :deleted_at,
    :deleted_at_i,
    :updated_at,
    :updated_at_i,
    :read_at,
    :read_at_i,
    :receipt_client_id,
    :retailer_id_s,
    :total,
    :user_id_s

json.pdf_url("/#{namespace.present? ? "#{namespace}/" : ''}receipts/#{receipt.id}.pdf")
json.text_url("/#{namespace.present? ? "#{namespace}/" : ''}receipts/#{receipt.id}.txt")
json.html_url("/#{namespace.present? ? "#{namespace}/" : ''}receipts/#{receipt.id}.html")
json.starred receipt.starred || false
