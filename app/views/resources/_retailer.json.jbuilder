json.extract! retailer,
    :id_s,
    :name,
    :created_at,
    :created_at_i,
    :updated_at,
    :updated_at_i,
    :category,
    :logo_image_url
