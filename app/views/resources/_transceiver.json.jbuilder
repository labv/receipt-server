json.extract! transceiver,
    :id_s,
    :created_at,
    :created_at_i,
    :updated_at,
    :updated_at_i,
    :token,
    :key

json.retailer do
    json.partial! :partial => '/resources/retailer', :locals => { :retailer => transceiver.retailer }
end

