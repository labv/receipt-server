# Retailer model
class Retailer
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include ModelHelpers

  field :name, :type => String
  field :category, :type => String
  field :logo_image_url, :type => String

  validates :name, :presence => true

  index :name => 1

  has_many :receipts
end

