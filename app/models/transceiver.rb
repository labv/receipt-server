# Transceiver model.
# Stores user authentication info
class Transceiver
  include Mongoid::Document
  include Mongoid::Timestamps
  include ModelHelpers

  before_validation :generate_token, :on => :create
  before_validation :generate_key, :on => :create

  field :token, :type => String
  field :key, :type => String
  field :last_accessed, :type => Time, :default => Time.now

  validates :token, :presence => true

  belongs_to :retailer
  has_many :receipts

  def generate_token
    self.token = SecureRandom.uuid.gsub(/-/, '')
  end

  def generate_key
    self.key = SecureRandom.uuid.gsub(/-/, '')
  end

  def touch!
    self.last_accessed = Time.now
    self.save!
  end

  def touch
    self.last_accessed = Time.now
    self.save
  end

  def auth_header
    ApiAuth::Authorization.new(self.token, self.key, self.id, 'TransceiverAuth').to_s
  end
end

