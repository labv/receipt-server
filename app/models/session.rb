# Session model.
# Stores user authentication info
class Session
  include Mongoid::Document
  include Mongoid::Timestamps
  include ModelHelpers

  before_validation :generate_session_token, :on => :create

  field :session_token, :type => String
  field :platform, :type => String
  field :device_id, :type => String
  field :push_token, :type => String
  field :last_accessed, :type => Time, :default => Time.now

  validates :session_token, :presence => true

  belongs_to :user, :index => true

  scope :pushable, -> {where(:push_token.exists => true)}

  after_save :cleanup_push_tokens

  def cleanup_push_tokens
    return if self.push_token.blank?

    Session.where(:push_token => self.push_token).where(:id.ne => id).each do |s|
      s.push_token = nil
      s.save
    end
  end

  def generate_session_token
    self.session_token = SecureRandom.uuid.gsub(/-/, '')
  end

  def touch!
    self.last_accessed = Time.now
    self.save!
  end

  def touch
    self.last_accessed = Time.now
    self.save
  end

  def user_id_s
    self.user_id.try(:to_s)
  end
end

