# User model
class User
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  include ModelHelpers

  before_create :generate_email_verification
  before_validation :fix_cases
  after_create :send_email_verification

  has_secure_password(validations: false)

  field :first_name, :type => String
  field :last_name, :type => String
  field :email, :type => String
  field :password_digest, :type => String
  field :dob, :type => Date
  field :accept_terms, :type => Boolean
  field :gender, :type => String
  field :email_verified, :type => Boolean, :default => false
  field :email_verification_token, :type => String
  field :password_reset_token, :type => String
  field :receipt_last_updated_at, :type => DateTime

  has_many :receipts
  has_many :sessions, :dependent => :delete

  index :email => 1
  index :email_verification_token => 1

  validates :email, :presence => true
  validates :email, uniqueness: { conditions: -> { where(deleted_at: nil) } }
  validates :email, :format => { :with => CommonRegex::EMAIL }

  validates :accept_terms, :acceptance => { :accept => true },
    :allow_nil => false

  validates :password,
    :length => { :minimum => 8 },
    :if => 'password_digest.nil? || password.present?'

  def send_email_verification
    if self.email_verification_token.nil?
      generate_email_verification
      self.save
    end

    UsersMailer.email_verification(self.email, self.email_verification_token).deliver
  end

  def fix_cases
    self.email = self.email.downcase
  end

  def generate_email_verification
    if self.email_verification_token.nil?
      self.email_verification_token = SecureRandom.uuid.gsub(/-/, '')
    end
  end

  def generate_password_reset!
    self.password_reset_token = SecureRandom.uuid.gsub(/-/, '')
    self.save!

    UsersMailer.reset_password(self.email, self.password_reset_token).deliver
  end

  def uidh
    raise "ID cannot be nil" if self.id.nil?
    Digest::SHA1.hexdigest(self.id)
  end

  def self.per_page
    10
  end
end

