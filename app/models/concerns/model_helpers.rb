module ModelHelpers
  def id_s
    self.id.try(:to_s)
  end

  def created_at_i
    self.created_at.try(&:to_i)
  end

  def updated_at_i
    self.updated_at.try(&:to_i)
  end

  def deleted_at_i
    self.deleted_at.try(&:to_i)
  end

  def read_at_i
    self.read_at.try(&:to_i)
  end

  def self.per_page
    30
  end

  def method_missing(method_sym, *arguments, &block)
    # the first argument is a Symbol, so you need to_s it if you want to pattern match
    if method_sym.to_s =~ /^(.*_id)_s$/
      self[$1.to_sym].to_s
    else
      super
    end
  end

  def self.respond_to?(method_sym, include_private = false)
    if method_sym.to_s =~ /^(.*_id)_s$/
      true
    else
      super
    end
  end
end

