module ExternalStorageFields

  def self.included(base)
    base.class_eval do
      extend ClassMethods
      prepend InstanceMethods

      def save(options = {})
        result = super(options)
        _save_external
        result
      end

      def _save_external
        storage = self.__external_storage_instance
        raise "storage not configured" if storage.nil?

        self.class.__external_fields.each do |field|
          if self.send("#{field}_changed?")
            value = self.send(field)
            if value.nil?
              if storage.exists?(external_storage_key(field))
                storage.delete(external_storage_key(field))
              end
            else
              storage.put(external_storage_key(field), value)
            end
          end
        end
      end
    end
  end

  module ClassMethods
    attr_reader :__external_storage
    attr_reader :__external_fields

    def set_external_storage(storage)
      unless storage.is_a? Proc
        raise "External storage should be a proc."
      end

      @__external_storage = storage
    end

    # Macro like method
    def external_field(name, options = {})
      name = name.to_sym
      @__external_fields ||= []
      @__external_fields << name

      _create_accessor(name)
      _create_dirty_method(name)

      name
    end

    private

    def _create_dirty_method(name)
      self.instance_variable_set("@__#{name}_set", false)
      define_method("#{name}_changed?") do
        self.instance_variable_get("@__#{name}_changed")
      end
    end

    def _create_accessor(name)
      self.instance_variable_set("@__#{name}_set", false)
      self.instance_variable_set("@__#{name}_changed", false)
      define_method("#{name}=") do |value|
        self.updated_at = Time.now
        self.instance_variable_set("@#{name}", value)
        self.instance_variable_set("@__#{name}_set", true)
        self.instance_variable_set("@__#{name}_changed", true)
      end

      define_method("#{name}_exists?") do
        self.__external_storage_instance.exists?(external_storage_key(name))
      end

      define_method(name) do
        if self.instance_variable_get("@__#{name}_set")
          self.instance_variable_get("@#{name}")
        else
          self.__external_storage_instance.get(external_storage_key(name))
        end
      end
    end
  end

  module InstanceMethods
    def __external_storage_instance
      if self.class.__external_storage.nil?
        return nil
      end

      instance = self.instance_variable_get("@__external_storage_instance")
      if instance.nil?
        instance = self.class.__external_storage.call
        self.instance_variable_set("@__external_storage_instance", instance)
      end

      instance
    end

    def reload
      self.class.instance_variable_get("@__external_fields").each do |name|
        self.instance_variable_set("@__#{name}_set", false)
      end
      super
    end

    def external_storage_key(name)
      raise "model not created yet" if self.id.nil?
      "#{self.id}_#{name.to_s}"
    end
  end
end

