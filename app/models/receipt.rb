# Receipt model
class Receipt
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include ModelHelpers
  include ExternalStorageFields

  STORAGE_PROVIDER = -> do
    ExternalStorageProviders::S3Provider.new(
      AppConfig.aws.access_key,
      AppConfig.aws.secret_key,
      AppConfig.aws.receipt_bucket_name,
      AppConfig.aws.receipt_base_path,
      Rails.logger
    )
  end

  set_external_storage STORAGE_PROVIDER

#  before_save :generate_pdf, :if => -> (receipt) do
#    !receipt.receipt_pdf_content_exists?
#  end

#  before_save :generate_text, :if => -> (receipt) do
#    !receipt.receipt_text_content_exists?
#  end
#
#  before_save :generate_html, :if => -> (receipt) do
#    !receipt.receipt_html_content_exists?
#  end

  after_save :update_user

  field :receipt_client_id, :type => String
  field :total, :type => BigDecimal
  field :read_at, :type => DateTime
  field :starred, :type => Boolean, :default => false
  field :pdf_generated, :type => Boolean, :default => false
  field :text_generated, :type => Boolean, :default => false
  field :html_generated, :type => Boolean, :default => false

  external_field :receipt_raw_content
  external_field :receipt_pdf_content
  external_field :receipt_text_content
  external_field :receipt_html_content

  belongs_to :user
  belongs_to :retailer, :index => true
  belongs_to :transceiver

  index :user_id => 1, :updated_at => -1

  def mark_as_read
    self.read_at = DateTime.now if self.read_at.nil?
  end

  def mark_as_read!
    mark_as_read
    self.save!
  end

  def is_read?
    !self.read_at.nil?
  end

  def update_user
    if self.user
      self.user.receipt_last_updated_at = self.updated_at
      self.user.save
    end
  end

  def receipt_content
    Base64.decode64(self.receipt_raw_content)
  end

  def generate_pdf
    if self.receipt_raw_content.present?
      self.receipt_pdf_content =
        Base64.encode64(Printers::EscPos::Pdf::PdfPrinter.new(Rails.logger).convert(self.receipt_content))
      self.pdf_generated = true
      self.save!
    end
  rescue => e
    Rails.logger.error("Failed to generate PDF: #{e.message}")
    Rails.logger.error(e.backtrace.join("\n"))
  end

  def generate_html
    generate_html!
  rescue => e
    Rails.logger.error("Failed to generate Html: #{e.message}")
    Rails.logger.error(e.backtrace.join("\n"))
  end

  def generate_html!
    if self.receipt_raw_content.present?
      self.receipt_html_content = Printers::EscPos::Html::HtmlPrinter.new(Rails.logger).convert(self.receipt_content)
      self.html_generated = true
      self.save!
    end
  end

  def generate_text
    generate_text!
  rescue => e
    Rails.logger.error("Failed to generate text: #{e.message}")
    Rails.logger.error(e.backtrace.join("\n"))
  end

  def generate_text!
    if self.receipt_raw_content.present?
      self.receipt_text_content = Printers::EscPos::Text::TextPrinter.new(Rails.logger).convert(
        self.receipt_content
      ).chars.select(&:valid_encoding?).join

      compute_total
      self.text_generated = true
      self.save!
    end
  end

  def compute_total
    regex = /total\D*(\d+.?,?\d+)/i
    text = self.receipt_text_content || ''
    text.split("\n").reverse_each do |line|
      result = line.match(regex)
      if result
        t = result.captures.first
        begin
          Float(t)
          self.total = BigDecimal.new(t, 5)
          return
        rescue
        end
      end
    end
  end

  def pdf_content
    return nil if self.receipt_pdf_content.nil?

    Base64.decode64(self.receipt_pdf_content)
  rescue => e
    Rails.logger.error("Failed to decode PDF data: #{e.message}")
    Rails.logger.error(e.backtrace.join("\n"))
    return nil
  end

  def append_raw_content!(new_content)
    if self.receipt_raw_content.nil?
      self.receipt_raw_content = Base64.encode64(new_content)
    else
      current_content = Base64.decode64(self.receipt_raw_content)
      self.receipt_raw_content = Base64.encode64(current_content + new_content)
    end

    self.save!
  end

  def generate_if_needed(force = false)
    self.generate_text if !self.text_generated || force
    self.generate_html if !self.html_generated || force
  end
end

