class Admin::TransceiversController < AdminBaseController
  before_action :_set_transceiver, :only => [:show, :destroy, :update]

  validates :page, :required => false
  validates :retailer_id, :required => false
  def index
    page = params[:page] || 1

    @transceivers = Transceiver
    if params[:retailer_id].present?
      @transceivers = @transceivers.where(:retailer_id => params[:retailer_id])
    end

    @transceivers = @transceivers.paginate(:page => page)
  end

  validates :retailer_id, :required => true
  def create
    @retailer = Retailer.find(params[:retailer_id])
    @transceiver = Transceiver.new(:retailer => @retailer)
    @transceiver.save!
  end

  def destroy
    @transceiver.destroy
    render :nothing => true
  end

  private

  def _set_transceiver
    @transceiver = Transceiver.find(params[:id])
  end
end

