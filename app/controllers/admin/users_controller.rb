class Admin::UsersController < AdminBaseController
  before_action :_set_user, :only => [:show, :destroy, :update]

  validates :page, :required => false
  def index
    page = params[:page] || 1
    @users = User.paginate(:page => page)
  end

  validates :first_name, :required => true
  validates :last_name, :required => true
  validates :email, :required => true, :format => CommonRegex::EMAIL
  validates :dob, :required => false, :transform => { :date => true }
  validates :accept_terms, :required => true, :transform => { :bool => true }
  validates :password, :required => true, :format => CommonRegex::PASSWORD
  def create
    @existing_user = User.where(:email => params[:email])
    if @existing_user.present?
      raise EmailInUseError.new
    end

    @user = User.new(@valid_params)
    @user.save!
  end

  def show
  end

  def destroy
    @user.destroy
    render :nothing => true
  end

  validates :first_name, :required => false
  validates :last_name, :required => false
  validates :email, :required => false, :format => CommonRegex::EMAIL
  validates :dob, :required => false, :transform => { :date => true }
  validates :accept_terms, :required => false, :transform => { :bool => true }
  validates :password, :required => false, :format => CommonRegex::PASSWORD
  def update
    @user.update!(@valid_params) if @valid_params.present?
  end

  private

  def _set_user
    @user = User.find(params[:id])
  end
end

