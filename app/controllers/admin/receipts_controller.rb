class Admin::ReceiptsController < AdminBaseController
  include ReceiptsControllerHelper

  before_action :_set_receipt, :only => [:show, :destroy, :update]

  validates :user_id, :required => false
  validates :retailer_id, :required => true
  validates :transceiver_id, :required => true
  validates :receipt_client_id, :required => true
  validates :receipt_content, :required => true
  def create
    @user = User.find(params[:user_id]) if params[:user_id].present?
    @retailer = Retailer.find(params[:retailer_id])
    @transceiver = Transceiver.find(params[:transceiver_id])
    content = if params[:receipt_content].respond_to?(:read)
                params[:receipt_content].read
              else
                params[:receipt_content]
              end
    bson_content = BSON::Binary.new(content, :generic)

    @receipt = Receipt.new(
      :user => @user,
      :retailer => @retailer,
      :transceiver => @transceiver,
      :receipt_client_id => params[:receipt_client_id],
      :receipt_raw_content => bson_content,
    )

    @receipt.save!
  end

  validates :page, :required => false
  def index
    page = params[:page] || 1
    @receipts = Receipt.all.paginate(:page => page)
    @receipts.each(&:generate_if_needed)
  end

  validates :user_id, :required => true
  def update
    @receipt.user_id = params[:user_id]
    @receipt.save!
  end

  private

  def _set_receipt
    @receipt = Receipt.find(params[:id])
  end
end

