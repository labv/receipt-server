class Admin::RetailersController < AdminBaseController
  before_action :_set_retailer, :only => [:show, :update, :destroy]

  validates :name, :required => true
  validates :category, :required => false
  def create
    @retailer = Retailer.where(:name => params[:name])
    if @retailer.present?
      raise RetailerNameInUseError.new
    end

    @retailer = Retailer.create(:name => params[:name])
  end

  def show
  end

  validates :name, :required => false
  def update
    @retailer.update!(@valid_params) if @valid_params.present?
  end

  validates :name, :required => false
  validates :name_pattern, :required => false, :transform => { :bool => true }
  def index
    query = {}
    if params[:name].present?
      query[:name] = params[:name] || ''
    end

    if params[:name_pattern] == true
      query[:name] = /#{query[:name]}.*/i
    end

    @retailers = if query.present?
                   Retailer.where(query)
                 else
                   Retailer.all
                 end.paginate
  end

  def destroy
    @retailer.destroy
    render :nothing => true
  end

  private

  def _set_retailer
    @retailer = Retailer.find(params[:id])
  end
end

