class UserAuthenticatedController < ApplicationController
  before_action :authenticate!

  def authenticate!
    auth_header = request.headers['Authorization']
    Rails.logger.info("Auth: #{auth_header}")

    auth = ApiAuth::Authorization.parse(auth_header)

    current_user = User.find(auth.user_id)
    @session = current_user.sessions.find_by(
      :id => auth.session_id,
      :session_token => auth.token
    )
    @session.touch

    @current_user = current_user
  rescue
    raise InvalidSessionError.new
  end
end
