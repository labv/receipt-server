class Transceiver::ReceiptsController < TransceiverBaseController
  before_action :_find_receipt, :only => [:upload_chunk, :get_chunk]

  validates :user_id, :required => false
  validates :receipt_client_id, :required => false
  validates :receipt_content, :required => true
  def create
    @user = User.find(params[:user_id]) if params[:user_id].present?
    @retailer = @current_transceiver.retailer
    content = if params[:receipt_content].respond_to?(:read)
                params[:receipt_content].read
              else
                params[:receipt_content]
              end

    b64_content = Base64.encode64(content)

    @receipt = Receipt.new(
      :user => @user,
      :retailer => @retailer,
      :transceiver => @current_transceiver,
      :receipt_client_id => params[:receipt_client_id],
      :receipt_raw_content => b64_content,
    )

    @receipt.save!

    if @user.present?
      Push::Gcm.push(
        @user.sessions.pushable.map(&:push_token),
        {},
        Push::Pokes::ReceiptChanges.new.to_h
      )
    end

    render :text => @receipt.id, :status => 200
  end

  validates :receipt_content, :required => true
  def upload_chunk
    content = if params[:receipt_content].respond_to?(:read)
                params[:receipt_content].read
              else
                params[:receipt_content]
              end

    @receipt.receipt_pdf_content = nil
    @receipt.receipt_text_content = nil
    @receipt.receipt_html_content = nil
    @receipt.append_raw_content!(content)
    @receipt.pdf_generated = false
    @receipt.text_generated = false
    @receipt.html_generated = false
    @receipt.save!

    render :text => @receipt.id, :status => 200
  end

  validates :offset, :required => false
  validates :length, :required => false
  def get_chunk
    if @receipt.transceiver != @current_transceiver
      Rails.logger.warn(
        "Invalid access to receipt: #{@receipt.id}. Transceiver ID: #{@current_transceiver.id}"
      )

      render :nothing, :status => :forbidden and return
    end

    offset = params[:offset]
    length = params[:length]

    if offset.nil? || length.nil?
      send_data (@receipt.receipt_content || '') and return
    end

    offset = offset.to_i
    length = length.to_i
    if offset < 0 || length <= 0
      send_data (@receipt.receipt_content || '') and return
    end

    total_length = @receipt.receipt_content.try(:bytes).try(:length) || 0
    response['Full-Content-Length'] = total_length
    content = (@receipt.receipt_content || '').bytes[offset..offset + length-1]
    response['Content-Range'] = "bytes #{offset}-#{offset + content.length - 1}"

    send_data (content.pack('c*') || '')
  end

  private

  def _find_receipt
    @receipt = Receipt.find(params[:id])
  end
end

