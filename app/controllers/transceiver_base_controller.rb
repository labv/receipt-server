class TransceiverBaseController < ApplicationController
  AUTH_TYPE = 'TransceiverAuth'
  before_action :authenticate!

  def authenticate!
    auth_header = request.headers['Authorization']
    Rails.logger.info("Auth: #{auth_header}")

    auth = ApiAuth::Authorization.parse(auth_header, AUTH_TYPE)

    @current_transceiver = Transceiver.find_by(
      :id => auth.user_id,
      :key => auth.session_id,
      :token => auth.token
    )
  rescue
    raise InvalidSessionError.new
  end
end

