class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # Only respond to json requests
  respond_to :json

  ##### Exception handling #####
  rescue_from Exception do |exception|
    raise exception if request.format.html?
    error = Rails.env == 'production' ? 'unknown error' : exception.message
    Rails::logger.error exception.message
    exception.backtrace.each { |line| Rails::logger.error line }
    render json: { error: InternalServerError.new(exception.message).to_hash }, status: :internal_server_error
  end

  rescue_from Mongoid::Errors::DocumentNotFound do |exception|
    raise exception if request.format.html?
    render json: { error: ObjectNotFoundError.new(exception).to_hash }, status: :not_found
  end

  rescue_from *(LabvErrors.all_exceptions) do |exception|
    raise exception if request.format.html?
    render json: { error: exception.to_hash }, status: exception.status_code
  end
end

