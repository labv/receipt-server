class RetailersController < UserAuthenticatedController
  before_action :_set_retailer, :only => [:show, :update, :destroy]
  def show
  end

  private

  def _set_retailer
    @retailer = Retailer.find(params[:id])
  end
end

