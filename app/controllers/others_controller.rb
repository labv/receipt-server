class OthersController < ActionController::Base
  def email_verification
    token = params[:token]
    if token.present?
      @user = User.where(:email_verification_token => token).first
      if @user.present?
        @user.email_verified = true
        @user.save!
      end
    end
  end
end

