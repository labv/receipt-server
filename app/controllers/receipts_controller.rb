class ReceiptsController < UserAuthenticatedController
  include ReceiptsControllerHelper
  LOOK_BACK_WINDOW = 30.minutes

  before_action :_set_receipt, :only => [:show, :destroy, :update]

  validates :page, :required => false
  validates :last_updated_at, :required => false
  def index
    user = @current_user
    page = params[:page] || 1
    @receipts = user.receipts.unscoped

    if params[:last_updated_at]
      last_updated = Time.at(params[:last_updated_at].to_i) - LOOK_BACK_WINDOW
      @receipts = @receipts.where(:updated_at.gte => last_updated)
    end

    @receipts = @receipts.desc(:updated_at).paginate(:page => page)
    @receipts.each(&:generate_if_needed)
  end

  validates :user_id, :required => false
  validates :retailer_id, :required => true
  validates :receipt_client_id, :required => true
  validates :receipt_content, :required => true
  def create
    @user = User.find(params[:user_id]) if params[:user_id].present?
    @retailer = Retailer.find(params[:retailer_id])

    @receipt = Receipt.new(
      :user => @user,
      :retailer => @retailer,
      :receipt_client_id => params[:receipt_client_id],
      :receipt_raw_content => params[:receipt_content],
    )

    @receipt.save!
  end

  validates :starred, :required => false
  validates :read, :required => false
  validates :user_id, :required => false
  def update
    if params[:user_id].present?
      if params[:user_id] != @current_user.id.to_s
        message = "Claiming user #{params[:user_id]} does not match caller's user ID #{@current_user.id}"
        Rails.logger.warn(message)
        raise InvalidParametersError.new message
      end
      @receipt.user_id = params[:user_id]
    end

    if params[:starred].present?
      @receipt.starred = params[:starred]
    end

    if params[:read] == true
      @receipt.mark_as_read
    elsif params[:read] == false
      @receipt.read_at = nil
    end

    @receipt.save!
  end

  private

  def _set_receipt
    @receipt = Receipt.find(params[:id])
  end
end

