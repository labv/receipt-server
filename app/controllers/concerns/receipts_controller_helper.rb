module ReceiptsControllerHelper
  def show
    @receipt.generate_if_needed

    respond_to do |format|
      format.json
      format.text do
        if @receipt.receipt_text_content.nil?
          head :not_found and return
        end

        send_data @receipt.receipt_text_content, :type => 'text/plain', :disposition => 'inline'
      end
      format.html do
        if @receipt.receipt_html_content.nil?
          head :not_found and return
        end

        send_data @receipt.receipt_html_content, :type => 'text/html', :disposition => 'inline'
      end
      format.pdf do
        if @receipt.pdf_content.nil?
          head :not_found and return
        end

        send_data @receipt.pdf_content, :type => 'application/pdf', :disposition => 'inline'
      end
    end
  end

  def destroy
    @receipt.destroy
    render :nothing => true
  end

end

