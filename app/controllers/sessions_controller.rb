class SessionsController < UserAuthenticatedController
  skip_before_action :authenticate!, :only => [:create, :reset_password]

  validates :email, :required => true
  validates :password, :required => true
  validates :platform, :required => true
  validates :device_id, :required => false
  def create
    @user = User.where(:email => params[:email]).first
    if @user.blank? || @user.authenticate(params[:password]) == false
      raise InvalidCredentialError.new
    end

    spec = @valid_params.merge({:user => @user})
    spec.delete(:email)
    spec.delete(:password)
    @session = Session.create!(spec)
  end

  validates :push_token, :required => true
  def update
    @session.push_token = @valid_params[:push_token]
    @session.save!
  end

  validates :email, :required => true
  def reset_password
    @user = User.where(:email => params[:email]).first
    if @user.present?
      @user.generate_password_reset!
    end

    render :nothing => true
  end

  def destroy
    @session.destroy!
    render :nothing => true
  end
end

