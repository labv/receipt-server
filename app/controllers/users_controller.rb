class UsersController < UserAuthenticatedController
  before_action :_set_user, :only => [:show, :destroy, :update]
  skip_before_action :authenticate!, :only => [:create, :reset_password_form, :reset_password]

  validates :email, :required => true
  def index
    User.find_by(:email => params[:email].downcase)
    render :nothing => true, :status => 200
  end

  validates :first_name, :required => true
  validates :last_name, :required => true
  validates :platform, :required => true
  validates :email, :required => true, :format => CommonRegex::EMAIL
  validates :dob, :required => false, :transform => { :date => true }
  validates :accept_terms, :required => true, :transform => { :bool => true }
  validates :password, :required => true, :format => CommonRegex::PASSWORD
  def create
    @existing_user = User.where(:email => params[:email])
    if @existing_user.present?
      raise EmailInUseError.new
    end

    p = @valid_params
    p.delete(:platform)
    @user = User.new(@valid_params)
    @user.save!
    @session = Session.create!(:user => @user, :platform => params[:platform])
  end

  def show
  end

  def destroy
    @user.destroy
    render :nothing => true
  end

  validates :first_name, :required => false
  validates :last_name, :required => false
  validates :email, :required => false, :format => CommonRegex::EMAIL
  validates :dob, :required => false, :transform => { :date => true }
  validates :accept_terms, :required => false, :transform => { :bool => true }
  validates :old_password, :required => false, :format => CommonRegex::PASSWORD
  validates :password, :required => false, :format => CommonRegex::PASSWORD
  def update
    if @valid_params[:password].present?
      if @valid_params[:old_password].present?
        if !@user.authenticate(@valid_params[:old_password])
          raise InvalidCredentialError.new
        end
      else
        raise InvalidParametersError.new(
          :old_password => ['old password needed']
        )
      end
    end

    @existing_user = User.where(:email => params[:email], :id => { '$ne' => @user.id })
    if @existing_user.present?
      raise EmailInUseError.new
    end

    @valid_params.delete(:old_password)

    @user.update!(@valid_params) if @valid_params.present?
  end

  validates :token, :required => true
  def reset_password_form
    @user = User.where(:password_reset_token => params[:token]).first
    @token = params[:token]
  end

  validates :token, :required => true
  validates :new_password, :required => true
  validates :new_password_confirm, :required => true
  def reset_password
    @user = User.where(:password_reset_token => params[:token]).first
    @token = params[:token]

    if params[:new_password] != params[:new_password_confirm]
      @error = "Passwords do not match"
      render :reset_password_form and return
    end

    if @user.present?
      @user.password = params[:new_password]
      @user.password_reset_token = nil
      @user.save
    else
      render :reset_password_form and return
    end
  end

  private

  def _set_user
    @user = User.find(params[:id])
    unless @current_user == @user
      raise IncorrectUserError.new
    end
  end
end

