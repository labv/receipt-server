module AnalyticsHelper
  def path_to_tracking(type, uidh, options = {})
    "#{AppConfig.ana_host}/t.gif?#{AnalyticsTracking.build_tracking_options(type, uidh, options).to_query}"
  end
end

