class UsersMailer < ActionMailer::Base
  default :from => 'TreeBox <no-reply@gettreebox.com>'

  def email_verification(email, token)
    @token = token
    mail(:to => email, :subject => 'Welcome to TreeBox!')
  end

  def reset_password(email, token)
    @token = token
    mail(:to => email, :subject => 'TreeBox Password Recovery')
  end
end

