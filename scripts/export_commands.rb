#!/usr/bin/env ruby
#
# Load Rails
ENV['RAILS_ENV'] = ARGV[0] || 'production'
DIR = File.dirname(__FILE__) 
require DIR + '/../config/environment'

lookup = {
  :esc => "\x1b",
  :fs => "\x1c",
  :gs => "\x1d",
  :dle => "\x10",
}

def to_hex(input, expected_length)
  result = nil
  if input.is_a? Integer
    result = [sprintf("%02X", input)]
  elsif input.is_a? String
    result = input.bytes.map do |char|
      to_hex(char, -1).first
    end
  else
    debugger
    raise "unexpected type"
  end

  if expected_length > 0
    if result.length != expected_length
      (expected_length - result.length).times do
        result << "00"
      end
    elsif result.length > expected_length
      raise "too long"
    end
  end

  result
end

max_length = nil
Printers::EscPos::Commands::FixedLengthCommands::COMMANDS.values.map(&:keys).flatten.each do |cmd|
  max_length = [max_length || cmd.bytes.length, cmd.bytes.length].max
end

max_length += 1

puts "Max length: #{max_length}\n"

results = []
Printers::EscPos::Commands::FixedLengthCommands::COMMANDS.each do |type, cmds|
  cmds.each do |cmd, (name, arg_length)|
    result_hex = [] # length, arg_length, cmd

    cmd = lookup[type] + cmd
    length_hex = to_hex(cmd.length, 1)
    result_hex += length_hex

    arg_length_hex = to_hex(arg_length, 4)
    result_hex += arg_length_hex

    cmd_hex = to_hex(cmd, max_length)
    result_hex += cmd_hex

    results << result_hex
    result_data = result_hex.map {|h| "0x#{h}"}.join(', ')
    puts "{ #{result_data} }, // #{name.to_s.titleize}"
  end
end

