# Overview

Receipt app server that provides data storage and API support to receipt transceiver and mobile client applications

## Requirement

### ImageMagick
sudo apt-get install imagemagick

### Tesseract
sudo apt-get install tesseract-ocr

sudo apt-get install tesseract-ocr-eng

### Ruby
Version: 2.0.0-p594

## Setup Guide
### Install Ruby
Use rbenv (https://github.com/sstephenson/rbenv) or rvm (https://rvm.io/)

#### rbenv guide
First install rbenv: https://github.com/sstephenson/rbenv
Then execute the following commands:
```
rbenv install 2.0.0-p594
```

### Install Bundler
```
gem install bundler
```

### Install all gems
```
cd ~/project_root/
bundle install
```

### Start the server
```
rails start
```
Open browser and go to http://localhost:3000

### User Authentication
PUT /sessions with email and password will return session ID, session token and user ID

Attach the following HTTP header

```
Authorization: ApiToken uid={user_id}, sid={session_id}, token={session_token}
```

### Transceiver Authentication
Every transceiver device has a *transceiver id* (*user_id*), *transceiver key* and *transceiver token*

Attach the following HTTP header to every request

```
Authorization: TransceiverAuth uid={user_id}, sid={transceiver_key}, token={transceiver_token}
```

## TODO

Things you may want to cover:

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...